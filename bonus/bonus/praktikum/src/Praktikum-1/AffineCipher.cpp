#include <cassert>
#include "AffineCipher.h"

// #decrypt()
int AffineCipher::decrypt
(
 const vector<byte>& cipher_text,
 const vector<byte>&  key,
 vector<byte>& plain_text)
{
    // Validiere Geheimtext, nur a-z ist erlaubt
    // cout << "cipher_text validieren (nur a-z erlaubt)" << endl;
    vector<byte>::const_iterator iter;
    for (iter = cipher_text.begin(); iter != cipher_text.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: cipher_text invalid:" << endl;
            return 0;
        }
    }

    // K = {(a, b) ∈ Z 26 × Z 26 | gcd(a, 26) = 1}
    //cout << "Schlüssel (a,b) validieren (a und b aus 0-25 und gcd(a, 26) = 1)" << endl;
    if(!(key[0] >= 0 && key[0] < 26 && key[1] >= 0 && key[1] <= 26 && inverse_table[key[0]] != 0)) {
        cout << "ERROR: Schlüssel invalid a:" << (int)key[0] << " b:" << (int)key[1] << endl;
        return 0;
    }



    // dec((a, b), y) = a −1 (y − b) mod 26, wobei (a, b) ∈ K
    int i = 0;
    for (iter = cipher_text.begin(); iter != cipher_text.end(); ++iter) {
        int result = (((int)inverse_table[key[0]] * ((int)(*iter) - (int)key[1])) % 26);
        // result kann Negativ sein. Bevor typecast zu byte, positiv machen -> sonst underflow
        if(result < 0) {
            result += 26;
        }
        plain_text.push_back(result);
        i++;
    }

    return i;
} // decrypt()


// #encrypt()
int AffineCipher::encrypt
(
 const vector<byte>& plain_text,
 const vector<byte>& key, 
 vector<byte>& cipher_text
)
{
    // P = C = Z 26 (a-z)
    // Validiere Klartext, nur a-z ist erlaubt
    //cout << "Klartext validieren (nur a-z erlaubt)" << endl;
    vector<byte>::const_iterator iter;
    for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: Klartext invalid:" << endl;
            return 0;
        }
    }

    // K = {(a, b) ∈ Z 26 × Z 26 | gcd(a, 26) = 1}
    //cout << "Schlüssel (a,b) validieren (a und b aus 0-25 und gcd(a, 26) = 1)" << endl;
    if(!(key[0] >= 0 && key[0] < 26 && key[1] >= 0 && key[1] <= 26 && inverse_table[key[0]] != 0)) {
        cout << "ERROR: Schlüssel invalid" << endl;
        return 0;
    }

    // enc((a, b), x) = ax + b mod 26, wobei (a, b) ∈ K
    int i = 0;
    for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
        cipher_text.push_back((key[0] * *iter + key[1]) % 26);
        i++;
    }

    return i;
} // encrypt()


// #multInverse()
int AffineCipher::multInverse(byte a) const {
  if (a<26) {
    return inverse_table[a];
  } // if
  else {
    return 0;
  } // else
} // multInverse()


// #inverse_table
const byte AffineCipher::inverse_table[26] = { 
  0,  // 0
  1,  // 1
  0,  // 2
  9,  // 3
  0,  // 4
  21, // 5
  0,  // 6
  15, // 7
  0,  // 8
  3,  // 9
  0,  // 10
  19, // 11
  0,  // 12
  0,  // 13
  0,  // 14
  7,  // 15
  0,  // 16
  23, // 17
  0,  // 18
  11, // 19
  0,  // 20
  5,  // 21
  0,  // 22
  17, // 23
  0,  // 24
  25  // 25
};
