#ifndef _AffineCipher_h_
#define _AffineCipher_h_

#include "ClassicCipher.h"

/**
 * @class A class for the affine cipher.
 * @author Bodo Brand
 */
class AffineCipher : public ClassicCipher {
private:
  /**
   * Die multiplikative Inverse-Tabelle von den Zahlen 0 bis 25 (mod 26).
   */
  const static byte inverse_table[26];

public:
  /**
   * Liest Geheimtext und Schlüssel und schreibt den Klartext in plain_text.
   * @param cipher_text Zeiger auf das Array mit dem Geheimtext
   * @param key Zeiger auf das Array mit dem Schlüssel
   * @param plain_text Zeiger auf das Array mit dem Klartext
   * @return Anzahl der entschlüsselten Bytes
   */
  virtual int decrypt(const vector<byte>& cipher_text, 
                      const vector<byte>&  key,
                      vector<byte>& plain_text);

   /**
    * Liest Klartext und Schlüssel und schreibt den Geheimtext in cipher_text.
    * @param plain_text Zeiger auf das Array mit dem Klartext
    * @param key Zeiger auf das Array mit dem Schlüssel
    * @param cipher_text Zeiger auf das Array mit dem Geheimtext
    * @return Anzahl der verschlüsselten Bytes
    */
  virtual int encrypt(const vector<byte>& plain_text,
                      const vector<byte>& key, 
                      vector<byte>& cipher_text);


  /**
   * Gibt das multiplikative Inverse zurück, anhand der inverse_table.
   * @param a Zahl
   * @return Multiplikatives Inverses
   * @retval 0 Wenn es kein multiplikatives Inverses für a gibt
   */
  int multInverse(byte a) const;

}; // AffineCipher

#endif
