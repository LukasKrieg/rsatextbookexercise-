#ifndef _AffineBreaker_h_
#define _AffineBreaker_h_

#include <map>
#include "AffineCipher.h"
#include "byte.h"

using namespace std;

/**
 * @class A class for breaking the affine cipher.
 * @author Bodo Brand
 */
class AffineBreaker : public AffineCipher {
protected:
  /**
   * Die Wahrscheinlichkeiten, wie oft ein Buchstabe im Geheimtext vorkommt.
   */
  multimap<float,byte> frequency_table;

  /**
   * Auf dem Geheimtext wird eine Häufigkeitsanalyse durchführt und in frequency_table gespeichert.
   * @param cipher_text Zeiger auf das Array mit dem Geheimtext
   */
  void computeFrequency(vector<byte>& cipher_text);

  /**
   * Diese Funktion berechnet anhand von ce und ct die Parameter a und b (also den Schlüssel).
   * Wenn der Schlüssel korrekt ist, dann werden die ersten 50 Buchstaben des potentiellen Klartextes ausgeben.
   *
   * Ein Schlüssel ist korrekt wenn: \n
   * - a ein multiplikatives Inverses hat bzw. gcd(a, 26) = 1 bzw. a ist teilerfremd zu 26
   * - (a * 4 + b) % 26 = ce, wobei 4 die Position von e im Alphabet (gezählt von 0 aufwärts)
   * - (a * 19 + b) % 26 = ct, wobei 19 die Position von t im Alphabet (gezählt von 0 aufwärts)
   *
   * Die Berechnung der Kongruenz Gleichungssystems via Substraktionsverfahren: \n
   * (I)  a * 4 + b ≡ 11 (mod 26) \n
   * (II) a * 19 + b ≡ 14 (mod 26) \n
   *
   * Berechnen von a: \n
   * (II) - (I) \n
   * (III) 15a ≡ 3 (mod 26)  | *7 \n
   * -> Die Linke Seite ist immer 15a \n
   * -> deshalb einfach das multiplikative Inverse von 15 auf beiden Seite dazu multiplizieren. \n
   * -> damit fällt die 15 weg \n
   * (III)  a ≡ 21 (mod 26) \n
   *
   * Formel für a:  a = ((ct - ce) * 7) % 26 \n
   *
   * Berechnen von b: \n
   * (III) in (I) einsetzen: \n
   * (IIII)  21 * 4 + b ≡ 11 (mod 26) | -11 | -b | *-1 \n
   *
   * (IIII)  -21 * 4 + 11 ≡ b (mod 26) \n
   * (IIII') b ≡ -21*4 + 11 (mod 26) \n
   * (IIII') b ≡ 5 (mod 26) \n
   *
   * Formel für b = (-(a * 4) + ce) % 26 \n
   *
   * @param ce die neue vermutete Position von Buchstabe e (vorher 4 nun z.B. 11)
   * @param ct die neue vermutete Position von Buchstabe t (vorher 19 nun z.B. 14)
   * @param cipher_text Zeiger auf das Array mit dem Geheimtext
   */
  void breakCipher(byte ce, byte ct, const vector<byte>& cipher_text);

public:
  /**
   * Zur Analyse des Geheimtexts werden alle Kombinationen der 5 häufigsten Buchstaben herangezogen.
   * Dies sind insgesamt 25 Buchstabenpaare.
   * @param cipher_text Zeiger auf das Array mit dem Geheimtext
   * @param nr_chars wird nicht benutzt
   */
  void analyse(vector<byte>& cipher_text, int nr_chars=5);
};

#endif
