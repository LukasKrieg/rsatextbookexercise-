#include <algorithm>
#include "AffineBreaker.h"

#include <set>

using namespace std;

// #analyse()
void AffineBreaker::analyse(vector<byte>& cipher_text, int nr_chars) {

    computeFrequency(cipher_text);

    //multimap ist nach first key sortiert
    multimap<float,byte>::reverse_iterator riter;
    int i = 0;
    for (riter = frequency_table.rbegin(); riter != frequency_table.rend(); ++riter) {
        if(i >= 5) break;
        int j = 0;
        multimap<float,byte>::reverse_iterator riter2;
        for (riter2 = frequency_table.rbegin(); riter2 != frequency_table.rend(); ++riter2) {
            if(j >= 5) break;
            if(riter->second != riter2->second) {
                breakCipher(riter->second, riter2->second, cipher_text);
            }
            j++;
        }
        i++;
    }
} // analyse()


// #breakCipher()
void AffineBreaker::breakCipher
(
 byte ce,
 byte ct,
 const vector<byte>& cipher_text
 )
{
    vector<byte> key;
    vector<byte> plain_text;

    //cout << "ce:" << (char)(ce+97) << " ct:" << (char)(ct+97) << endl;
    int a = (((int)ct - (int)ce) * 7) % 26;
    if(a < 0) a += 26;

    if(!multInverse((byte)a)) return;

    int b = (-(a * 4) + (int)ce) % 26;
    if(b < 0) b += 26;

    key.push_back(a);
    key.push_back(b);

    // erste 50 Buchstaben des möglichen Klartextes Ausgeben
    if(decrypt(cipher_text, key, plain_text)) {
        vector<byte>::iterator iter;
        int i = 0;
        for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
            if(i >= 50) break;
            cout << (char)((*iter)+97);
            i++;
        }

        cout << endl;
    }

    /* Bruteforce Variante
    cout << "ce:" << (char)(ce+97) << " ct:" << (char)(ct+97) << endl;
    for(int a = 0; a < 26; a++) {
        // Wenn a kein multiplikatives Inverses, dann überspringen
        if(!multInverse(a)) continue;

        //cout << " " << a << " ";

        for(int b = 0; b < 26; b++) {
            if((a * 4 + b) % 26 != (int)ce) {
                continue;
            }

            if((a * 19 + b) % 26 != (int)ct) {
                continue;
            }

            vector<byte> key;
            key.push_back(a);
            key.push_back(b);

            //cout << "Possible Solution: a:" << a << " b:" << b << " " << endl;
            // erste 50 Buchstaben des möglichen Klartextes Ausgeben
            vector<byte> plain_text;
            if(decrypt(cipher_text, key, plain_text)) {
                vector<byte>::iterator iter;
                int i = 0;
                for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
                    if(i >= 50) break;
                    cout << (char)((*iter)+97);
                    i++;
                }

                cout << endl;
            }
        }

    }*/

} // breakCipher()

// #computeFrequency()
void AffineBreaker::computeFrequency(vector<byte>& cipher_text) {
    //cout << endl;

    map<byte,int> buchstabenCount;
    vector<byte>::iterator iterInput;
    for (iterInput = cipher_text.begin(); iterInput != cipher_text.end(); ++iterInput) {
        if(buchstabenCount.count(*iterInput) == 0) {
            buchstabenCount.insert(pair<byte, int>(*iterInput, 1));
        } else {
            buchstabenCount[*iterInput]++;
        }
    }

    //cout << "Buchstaben Vorkommen:" << endl;
    map<byte, int>::iterator map_it2;
    map_it2 = buchstabenCount.begin();
    while (map_it2!=buchstabenCount.end()) {
        //cout << (char)(map_it2->first+97) << " => " << map_it2->second << endl;
        frequency_table.insert(pair<float, int>((float)map_it2->second / (float)cipher_text.size(), map_it2->first));
        map_it2++;
    }
    //cout << endl;

    //cout << "Buchstaben Häufigkeit:" << endl;
    multimap<float, byte>::iterator multimap_it2;
    multimap_it2 = frequency_table.begin();
    while (multimap_it2!=frequency_table.end()) {
        //cout << multimap_it2->first << " => " << (char)(multimap_it2->second+97) << endl;
        multimap_it2++;
    }
} // computeFrequency()

