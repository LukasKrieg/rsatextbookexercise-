/**
 * \file stldemo.cpp
 * \brief Beispielprogramme zur STL
 */

#include <iostream>
#include <vector>
#include <set>
#include <map>

#include "byte.h"

using namespace std;

/**
 * STL Demo Aufgaben: iterator, set, multiset, map, multimap und Buchstaben Häufigkeiten.
 * @return Returncode
 * @retval 0 kein Fehler ist aufgetreten
 */
int main() {
	cout << "STL Demo Application" << endl;

    cout << "Aufgabe 2) " << endl;
    int int_arr[7] = { 4, 3, 6, 12, 5, 2, 8 };
    vector<int> v(int_arr, int_arr+7);
    // vector<int> v { 4, 3, 6, 12, 5, 2, 8 };   c++17 ? but we should use c++11

    // Forward Iterator
    cout << "Forward Iterator:" << endl;
    vector<int>::iterator v_it;
    v_it = v.begin();
    while (v_it!=v.end()) {
        cout << *v_it << endl;
        v_it++;
    }

    // Iterate and print values of vector
    //for(int n : v) {
    //    std::cout << n << '\n';
    //}

    // Backward Iterator
    cout << "Backward Iterator:" << endl;
    vector<int>::reverse_iterator v_rit;
    v_rit = v.rbegin();
    while (v_rit!=v.rend()) {
        cout << *v_rit << endl;
        v_rit++;
    }

    cout << endl;

    cout << "Aufgabe 3)" << endl;
    cout << "Verwendung von set:" << endl;

    int int_arr2[8] = { 4, 3, 6, 12, 12, 5, 2, 8 };
    set<int> s(int_arr2, int_arr2+8);

    cout << "Forward Iterator:" << endl;
    set<int>::iterator s_it;
    s_it = s.begin();
    while (s_it!=s.end()) {
        cout << *s_it << endl;
        s_it++;
    }

    cout << "Backward Iterator:" << endl;
    set<int>::reverse_iterator s_rit;
    s_rit = s.rbegin();
    while (s_rit!=s.rend()) {
        cout << *s_rit << endl;
        s_rit++;
    }

    // Frage: Wie oft kommt das Element 12 in der Menge vor?
    // Antwort: Das Element 12 kommt nur einmal vor, da es eine Menge ist.

    // Frage: Was passiert, wenn man anstatt set die Datenstruktur multiset verwendet?
    cout << "Verwendung von multiset:" << endl;
    multiset<int> ms(int_arr2, int_arr2+8);

    cout << "Forward Iterator:" << endl;
    multiset<int>::iterator ms_it;
    ms_it = ms.begin();
    while (ms_it!=ms.end()) {
        cout << *ms_it << endl;
        ms_it++;
    }

    cout << "Backward Iterator:" << endl;
    multiset<int>::reverse_iterator ms_rit;
    ms_rit = ms.rbegin();
    while (ms_rit!=ms.rend()) {
        cout << *ms_rit << endl;
        ms_rit++;
    }
    // Antwort: Elemente können mehrfach vorkommen. Die 12 also 2 mal.

    cout << "Aufgabe 4)" << endl;
    cout << "Verwendung von map:" << endl;
    map<int, string> m;
    m.insert(pair<int, string>(11, "Materie"));
    m.insert(pair<int, string>(7, "eine"));
    m.insert(pair<int, string>(4, "ist"));
    m.insert(pair<int, string>(1, "Kryptografie"));
    m.insert(pair<int, string>(9, "spannende"));
    m.insert(pair<int, string>(7, "sehr"));

    cout << "Forward Iterator:" << endl;
    map<int, string>::iterator map_it;
    map_it = m.begin();
    while (map_it!=m.end()) {
        cout << map_it->first << " => " << map_it->second << endl;
        map_it++;
    }

    cout << "Backward Iterator:" << endl;
    map<int, string>::reverse_iterator map_rit;
    map_rit = m.rbegin();
    while (map_rit!=m.rend()) {
        cout << map_rit->first << " => " << map_rit->second << endl;
        map_rit++;
    }

    //Frage: Was passiert, wenn Sie anstatt map die Klasse multimap verwenden?
    cout << "Verwendung von multimap:" << endl;
    multimap<int, string> mm;
    mm.insert(pair<int, string>(11, "Materie"));
    mm.insert(pair<int, string>(7, "eine"));
    mm.insert(pair<int, string>(4, "ist"));
    mm.insert(pair<int, string>(1, "Kryptografie"));
    mm.insert(pair<int, string>(9, "spannende"));
    mm.insert(pair<int, string>(7, "sehr"));

    cout << "Forward Iterator:" << endl;
    multimap<int, string>::iterator multimap_it;
    multimap_it = mm.begin();
    while (multimap_it!=mm.end()) {
        cout << multimap_it->first << " => " << multimap_it->second << endl;
        multimap_it++;
    }

    cout << "Backward Iterator:" << endl;
    multimap<int, string>::reverse_iterator multimap_rit;
    multimap_rit = mm.rbegin();
    while (multimap_rit!=mm.rend()) {
        cout << multimap_rit->first << " => " << multimap_rit->second << endl;
        multimap_rit++;
    }
    // Antwort: multimap erlaubt mehrermals den selben Schlüssel

    cout << "Aufgabe 5)" << endl;
    // Einlesen der Buchstaben
    string input = "diesisteinextremlangertextderallebuchstabenenthaeltauszereinpaarwenigendienichtenthaltensindwasindiesemfalljedochkeinproblemdarstellt";

    map<byte,int> buchstabenCount;
    string::iterator iterInput;
    for (iterInput = input.begin(); iterInput != input.end(); ++iterInput) {
        cout << *iterInput;
        if(buchstabenCount.count(*iterInput) == 0) {
            buchstabenCount.insert(pair<byte, int>(*iterInput, 1));
        } else {
            buchstabenCount[*iterInput]++;
        }
    }

    cout << endl;

    multimap<float, byte> buchstabenHaufigkeit;

    cout << "Buchstaben Vorkommen:" << endl;
    map<byte, int>::iterator map_it2;
    map_it2 = buchstabenCount.begin();
    while (map_it2!=buchstabenCount.end()) {
        cout << map_it2->first << " => " << map_it2->second << endl;
        buchstabenHaufigkeit.insert(pair<float, int>((float)map_it2->second / (float)input.length(), map_it2->first));
        map_it2++;
    }

    cout << "Buchstaben Häufigkeit:" << endl;
    multimap<float, byte>::iterator multimap_it2;
    multimap_it2 = buchstabenHaufigkeit.begin();
    while (multimap_it2!=buchstabenHaufigkeit.end()) {
        cout << multimap_it2->first << " => " << multimap_it2->second << endl;
        multimap_it2++;
    }


	return 0;
}
