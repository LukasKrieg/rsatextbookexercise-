#ifndef _DESCipher_h_
#define _DESCipher_h_

#include "BlockCipher.h"

class DESCipher : public BlockCipher {
private:
    /** Initial permutation IP. */
    static byte ip[64];

    /** Final permutation IP^{-1}. */
    static byte fp[64];

    /** Expansion vector. */
    static byte ev[48];

    /** Permuted choice 1. */
    static byte pc1[];

    /** Permuted choice 2. */
    static byte pc2[];

    /** S-Boxes 1-8. */
    static byte sbox[8][64];

    /** Permutation p. */
    static byte pp[32];

    /** Total rotations in key schedule. */
    static byte total_rot[16];

    /** Key schedule. */
    byte key_schedule[16][6];

public:

    /**
     * Konstruktur.
     */
    DESCipher();

    /**
     * Destruktur.
     */
    ~DESCipher();

    /**
     * Compute the key schedule. The 64 bit key is used to calculate 16 round keys (each 48 bit).
     * For that purpose two permutation tables and a number of shifts is applied to get each round key.
     *
     * @param key 64bit key for generation of the 16 round keys.
     * @param encmode true => key schedule for encryption.
     *
     */
    void computeKeySchedule(const byte *key, bool encmode = true);

    /**
     * Compute S-Box substitution. All S-Boxes are given.
     * This function returns the value in the given line and column in the S-Box table with the id.
     *
     * @param id number of S-Box
     * @param line line in S-Box
     * @param col column in S-Box
     *
     * @return Value read from the S-Box table.
     */
    byte computeSBox(byte id, byte line, byte col);

    /**
     * Decrypt a cipher text in DES Electronic Code Book Mode. This means we split the text in multiple blocks
     * and process them each invidually.
     *
     * @param cipher_text array to store cipher text
     * @param cipher_len length of the cipher text array (multiple of 8 bytes)
     * @param key key
     * @param key_len key length
     * @param plain_text plain text to be decrypted
     * @param plain_len length of the plain text
     *
     * @return number of decrypted bytes
     */
    virtual int decrypt(const byte *cipher_text, int cipher_len,
                        const byte *key, int key_len,
                        byte *plain_text, int plain_len);

    /**
     * Encrypt a plain text in DES Electronic Code Book Mode. This means we split the text in multiple blocks
     * and process them each invidually.
     *
     * @param plain_text plain text to be encrypted
     * @param plain_len length of the plain text
     * @param key key
     * @param key_len key length
     * @param cipher_text array to store cipher text
     * @param cipher_len length of the cipher text array
     *
     * @return number of encrypted bytes
     */
    virtual int encrypt(const byte *plain_text, int plain_len,
                        const byte *key, int key_len,
                        byte *cipher_text, int cipher_len);

    /**
     * Processes one Block of 8 bytes.
     * First a initial permutation with the permutation table ip will be applied on the block.
     * Second the Feistelcipher will be applied for 16 rounds.
     * In the end a final permutation with the permutation table fp will be applied.
     *
     * @param in_block input block
     * @param out_block output block
     */
    void processBlock(const byte *in_block, byte *out_block);

    /**
     * Feistel chiffre as used in the DES.
     *
     * The following formula will be applied:\n
     * <pre>
     * r  = f/r     , k \ ⊕ l
     *  i    \ i - 1   i/    i - 1
     *
     * l  = r
     *  i    i - 1
     *
     * f is functionF
     * r is the right input
     * l the left input
     * k is the round key
     * </pre>
     *
     * One round of feistel is executed. Processblock will call it one time for each round.
     *
     * @param l_in left input (4 byte)
     * @param r_in right input (4 byte)
     * @param key round key (6 byte)
     * @param l_out left output (4 byte)
     * @param r_out right output (4 byte)
     * @param rnd round of DES (for debug output)
     */
    void feistel(const byte *l_in,
                 const byte *r_in,
                 const byte *key,
                 byte *l_out,
                 byte *r_out,
                 int rnd = 0);

    /**
     * Function f of the DES.
     * FunctionF takes the 48 bit round key and the 32 bit right input value from feistel (r).
     * r gets expanded to 48 bit with the expansion vector table. k and r will be Xored.
     * The resulting 48 bit will be seperated in 8 6bit values.
     * Each 6bit Value will be used to get a 4 bit Value from the corresponding SBox of the current round.
     *
     * e.g. w = 101110
     * row = (w1w6) = 10
     * column = (w2w3w4w5) = 0111
     *
     * All 8 4bit values from the SBoxes get concatenated to a 32 bit value.
     * The output of functionF is a 32bit Value after a final permutation with the permutation table p.
     *
     * @param r_in right input (4 byte)
     * @param key round key (6 byte)
     * @param r_out right output (4 byte)
     * @param rnd round of DES (for debug output)
     */
    void functionF(const byte *r_in,
                   const byte *key,
                   byte *r_out,
                   int rnd = 0);

    /**
     * Get the i-th bit of a byte array. A byte will be prepared with shifts. Afterwards an and operation gets the bit.
     *
     * @param array pointer to the byte array
     * @param array_len length of the array
     * @param pos position of the requested bit
     *
     * @return value of the bit at position pos
     */
    bool getBit(const byte *array, int array_len, int pos) const;

    /**
     * Compute a bit permutation of a byte array. The permutation table is used to determine the bit position in the
     * new array. This procedure is also called permutation.
     *
     * @param p pointer to the permutation table to use.
     * @param p_len length of the permutation table.
     * @param in_array input byte array.
     * @param in_len length of the input byte array.
     * @param out_array output byte array.
     * @param out_len length of the output byte array.
     *
     */
    void permutate(const byte *p, int p_len,
                   const byte *in_array, int in_len,
                   byte *out_array, int out_len) const;

    /**
     * Print a byte field as bit array on the screen.
     *
     * @param bytefield Pointer to the bytefield that will be printed.
     * @param len length of the bytefield
     * @param block_len length of one bit block.
     */
    void printBitField(const byte *bytefield, int len, int block_len = 8) const;

    /**
     * Set the i-th bit of a byte array. A byte will be prepared and with "or" or "xor" and "and" processed.
     *
     * @param array pointer to the byte array in which the bit will be set.
     * @param array_len length of the array in which the bit will be set.
     * @param pos position of the bit to be set in the array.
     * @param value Value the bit will be set to(1 or 0).
     */
    void setBit(byte *array, int array_len, int pos, bool value) const;


    /**
     * Kodiert die Bitvektoren 0010111101101011 und 1111001111000000 als Bytearray und
     * gibt Sie mittels printBitField() aus.
     */
    void aufgabe3test();
}; // class DESCipher

#endif
