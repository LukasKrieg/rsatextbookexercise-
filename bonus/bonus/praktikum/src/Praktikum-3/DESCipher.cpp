#include <cassert>
#include <iomanip>
#include "DESCipher.h"

// #DESCipher()
DESCipher::DESCipher() {
} // DESCipher()


// #~DESCipher()
DESCipher::~DESCipher() {
} // ~DESCipher()


// #computeKeySchedule() 
void DESCipher::computeKeySchedule(const byte *key, bool encmode) {
    //byte pc2_ls[48]; // permuted choice 2 with integrated left shifts

    // gegeben sind folgende Variablen:
    // 	key: 64 bit, 8 byte
    //  encmode: true -> Verschlüsselung
    // 	byte key_schedule[16][6]; -> Hier Ergebnis abspeichern, 16 Runden je 48 bit Schlüssel
    // 	static byte total_rot[16]; -> Anzahl der Rotationen in jeder Runde ls1, ls2, ...
    //  static byte pc1[]; -> Die Permutationstabelle pc1
    //  static byte pc2[]; -> Die Permutationstabelle pc2
    /**
     * Durch alle 16 Runden iterieren.
     */
    for (int round = 0; round < 16; round++) {
        /**
         * Permutationen (pc1 und pc2) und Linksshifts kombinieren zu einer Permutationstabelle
         * mit welcher sich der Schlüssel mit der Funktion permutate für die aktuelle Runde berechnen lässt.
         */
        byte perm_table[48] = {
                0}; // Permutationstabelle, welche den 56 bit auf den jeweiligen 48 bit Rundschlüssel mapped

        //cout << "Round " << round << ":" << endl;

        /**
         * Alle Werte in pc2[0:23] sind <= 28
         * total_rot ist >= 1.
         * Iteriere durch die ersten 24 Werte von pc2.
         */
        for (int i = 0; i < 24; i++) {
            /**
             * Statt den Linksshift auf pc1 anzuwenden, wird der Linksshift auf pc2 addiert, womit die korrekte
             * Position des entsprechenden Schlüsselbits aus pc1 ausgelesen wird.
             * perm_table[0] enthält somit die Bitposition im Ausgangsschlüssel die zum ersten Schlüsselbit des aktuellen Rundenschlüssels wird.
             *
             */
            perm_table[i] = pc1[(pc2[i] + total_rot[round] - 1) % 28];

            //Ausgabe der Permutationstabelle mit Anpassung der Länge von Zahlen.
            /*if((int)perm_table[i] < 10) {
                cout << (int)perm_table[i] << "  ";
            } // if
            else {
                cout << (int)perm_table[i] << " ";
            } // else
            if(((i+1) % 12) == 0) {
                cout << endl;
            } // if*/
        } // for

        /**
         * Alle Werte in pc2[24:48] sind > 28
         */
        for (int i = 24; i < 48; i++) {
            /**
             * Statt den Linksshift auf pc1 anzuwenden, wird der Linksshift auf pc2 addiert, womit die korrekte
             * Position des entsprechenden Schlüsselbits aus pc1 ausgelesen wird.
             * Da die Werte zwischen 29 und 56 liegen wird hier nach Modulo 28, 28 aufaddiert.
             */
            perm_table[i] = pc1[((pc2[i] + total_rot[round] - 1) % 28) + 28];

            //Ausgabe der Permutationstabelle mit Anpassung der Länge von Zahlen.
            /*
                  if((int)perm_table[i] < 10) {
                      cout << (int)perm_table[i] << "  ";
                  } // if
                  else {
                      cout << (int)perm_table[i] << " ";
                  } // else
                  if(((i+1) % 12) == 0) {
                      cout << endl;
                  } // if*/
        } // for
        //cout << endl;

        /**
         * Die Permutationstabelle wird angewendet, um den aktuellen Rundenschlüssel zu berechnen.
         * Insgesamt werden 16 rundenschlüssel berechnet.
         */
        if (encmode == true) {
            /**
             * Bei Verschlüsselung wird die Rundenschlüssel in normaler Reihenfolge berechnet und gespeichert.
             * Die Permutationstabelle der ersten Runde erzeugt somit den ersten Rundenschlüssel.
             */
            permutate(perm_table, 48, key, 8, key_schedule[round], 6);
        } // if
        else {
            /**
             * Bei Entschlüsselung werden die Rundenschlüssel in umgekehrter reihenfolge gespeichert.
             * Die Permutationstebelle der ersten Runde wird somit den letzten Rundenschlüssel erzeugen.
             */
            permutate(perm_table, 48, key, 8, key_schedule[15 - round], 6);
        } // else

    } // for
} // computeKeySchedule()


// #computeSBox()
byte DESCipher::computeSBox(byte id, byte line, byte col) {
    //gegeben:
    // sbox[8][64] -> 8 S-Boxen mit je 64 Byte
    return sbox[id][line * 16 + col];
} // computeSBox()


// #decrypt()
int DESCipher::decrypt
        (
                const byte *cipher_text,
                int cipher_len,
                const byte *key,
                int key_len,
                byte *plain_text,
                int plain_len
        ) {
    /**
     * Der cipher_text muss ein Vielfaches der Länge 8 sein.
     */
    assert((cipher_len % 8) == 0);
    /*cout << "cipher_len = " << cipher_len << endl;
    cout << "cipher_text in Binär:" << endl;
    printBitField(cipher_text, cipher_len);*/

    //Gebe Key in Binär aus
    /*cout << "Key in Binär:" << endl;
      printBitField(key, key_len);
      cout << endl;*/


    /**
     * Alle 16 Rundenschlüssel berechnen
     */
    computeKeySchedule(key, false);
    /*cout << "key_schedule Tabelle:" << endl;
    for(int i = 0; i < 16; i++) {
    cout << "Round " << i << ": ";
        printBitField(key_schedule[i], 6);
    } // for
    cout << endl;*/

    /**
     * Den Cipher Text in 8 byte Blöcke teilen und jeweils entschlüsseln.
     */
    byte current_block[8] = {0};
    byte decrypted_block[8] = {0};
    /**
     * Durch die Länge des Cipher Textes iterieren.
     */
    for (int i = 0; i < cipher_len; i++) {
        current_block[i % 8] = cipher_text[i];

        /**
         * Wenn Ein Block gefüllt wurde, dann wird dieser verarbeitet (entschlüsselt)
         */
        if ((i % 8) == 7) {

            processBlock(current_block, decrypted_block);

            /**
             * Die entschlüsselten Blöcke werden zu einem Array zusammengefügt.
             */
            plain_len += 8;

            for (int j = 0; j < 8; j++) {
                plain_text[((i / 8) * 8) + j] = decrypted_block[j];
            } // for
        } // if
    } // for

    return plain_len;
} // decrypt()


// #encrypt()
int DESCipher::encrypt
        (
                const byte *plain_text,
                int plain_len,
                const byte *key,
                int key_len,
                byte *cipher_text,
                int cipher_len) {
    /**
     * Der plain_text muss ein Vielfaches der Länge 8 sein.
     */
    assert((plain_len % 8) == 0);
    /*cout << "plain_len = " << plain_len << endl;
    cout << "plain_text in Binär:" << endl;
    printBitField(plain_text, plain_len);*/



    //Gebe Key in Binär aus.
    /*cout << "Key in Binär:" << endl;
    printBitField(key, key_len);
    cout << endl;*/

    /**
     * Die 16 Rundenschlüssel werden berechnet.
     */
    computeKeySchedule(key, true);
    /*cout << "key_schedule Tabelle:" << endl;
    for(int i = 0; i < 16; i++) {
    cout << "Round " << i << ": ";
        printBitField(key_schedule[i], 6);
    } // for
    cout << endl;*/

    /**
     * Den Plaintext in 8 byte Blöcke Teilen und jeweils verschlüsseln.
     */
    byte current_block[8] = {0};
    byte encrypted_block[8] = {0};
    for (int i = 0; i < plain_len; i++) {
        current_block[i % 8] = plain_text[i];

        /**
         * Wenn Ein Block gefüllt wurde, dann wird dieser verarbeitet (verschlüsselt)
         */
        if ((i % 8) == 7) {

            processBlock(current_block, encrypted_block);

            /**
             * Die verschlüsselten Blöcke werden zu einem Array zusammengefügt.
             */
            cipher_len += 8;

            for (int j = 0; j < 8; j++) {
                cipher_text[((i / 8) * 8) + j] = encrypted_block[j];
            } // for
        } // if
    } // for

    return cipher_len;
} // encrypt()


// #feistel()
void DESCipher::feistel
        (
                const byte *l_in,
                const byte *r_in,
                const byte *key,
                byte *l_out,
                byte *r_out,
                int rnd
        ) {

    // @param l_in left input (4 byte)
    // @param r_in right input (4 byte)
    // @param key round key (6 byte)
    // @param l_out left output (4 byte)
    // @param r_out right output (4 byte)
    // @param rnd round of DES (for debug output)

    /**
     * Die F Funktion wird auf r_in und key angewendet.
     * Siehe Dokumentation von functionF.
     */
    functionF(r_in, key, r_out, rnd);

    /**
     * Das Ergebnis von der F Funktion wird mit l_in xor verknüpft.
     */
    for (int i = 0; i < 4; i++) {
        r_out[i] = r_out[i] ^ l_in[i];
    } // for

    /**
     * Im letzten Schritt der Feistel Chiffre wird l_out auf r_in gesetzt.
     */
    for (int i = 0; i < 4; i++) {
        l_out[i] = r_in[i];
    } // for
} // feistel()


// #functionF()
void DESCipher::functionF
        (
                const byte *r_in,
                const byte *key,
                byte *r_out,
                int rnd
        ) {

    // Eingabe: r sind 32 bit, k sind 48 bit
    // 1. Expandiere r zu einem 48-Bitwort w := E(r).
    // 2. Berechne b := w ⊕ k und zerlege b in b 1 b 2 . . . b 8 , wobei b_i ein 6 bit Wort ist
    // 3. Berechne c_i := S i (b i ) für 1 ≤ i ≤ 8. Hierbei ist c i ∈ {0, 1} 4 .
    // 4. Berechne z := P(c 1 c 2 . . . c 8 ). Ausgabe: z = f(r, k) ∈ {0, 1} 32

    /**
     * Expandiere r_in mit der Tabelle E von 32 bit auf 48 bit.
     * Gebe das Ergebnis und den key aus.
     */
    byte r_in_ex[6] = {0};

    permutate(ev, 48, r_in, 4, r_in_ex, 6);
    /*cout << "E(r" << rnd << ")      = ";
    printBitField(r_in_ex, 6);
    cout << "k" << rnd+1 << "         = ";
    printBitField(key, 6);*/

    /**
     * Berechne b := w ⊕ k und gebe das Ergebnis aus.
     */
    byte b[6] = {0};
    for (int i = 0; i < 6; i++) {
        b[i] = r_in_ex[i] ^ key[i];
    } // for

    /*cout << "b = E(r" << rnd << ") ⊕ k" << rnd+1 << " = ";
    printBitField(b, 6);*/

    /**
     * zerlege b in b_1 b_2 . . . b_8 , wobei b_i 6 bit Wörter sind.
     */
    byte zerlegt[8] = {0}; // wir nutzen jeweils nur die ersten 6 bits von dem byte
    for (int i = 0; i < 48; i++) {
        //int byte_i = i/8;
        //int bit_i = i%8;

        /**
         * Die 6 bit Wörter werden jeweils in ein Byte geschrieben von dem die letzten 2 bit ignoriert werden.
         * Somit werden mit einer Formel alle 6 bit 2-bit übersprungen:\n
         * <pre>
         * 0(i=0), 1(i=1), 2(i=2), 3(i=3), 4(i=4), 5(i=5)
         * 8(i=6), 9(i=7),10(i=8),11(i=9),12(i=10),13(i=11)
         * 16(i=12),17(i=13),18(i=14)...
         * </pre>
         */
        setBit(zerlegt, 8, i + (2 * ((i + (2 * (i / 6))) / 8)), getBit(b, 6, i));
    } // for

    /*cout << "B in 6-bit = ";
    printBitField(zerlegt, 8);*/

    /**
     * Berechne c_i := S_i (b_i ) für 1 ≤ i ≤ 8. Hierbei sind c_i 4 bit Wörter.
     * Durchlaufe acht 6-bit Wörter und speichere die 4-bit Wörter in sbox_result.
     * Die 4 bit Wörter werden in Bytes gespeichert von dem die letzten 4 bit ignoriert werden.
     */
    byte sbox_result[8] = {0}; // acht 4-bit Wörter

    for (int i = 0; i < 8; i++) {
        //cout << "SBox " << i << ": " << endl;
        /**
         * line setzt sich aus dem 0.ten und 5.ten Bit zusammen
         * Beispiel:
         * 	010001(00) -> die letzten zwei ignorieren
         * 	00000001 das erste und sechste Bit an Position 7 und 8 setzen
         */
        byte line[1] = {0};
        setBit(line, 1, 6, getBit(zerlegt, 8, i * 8));
        setBit(line, 1, 7, getBit(zerlegt, 8, (i * 8) + 5));

        /*cout << "line = " << (int)(line[0]) << " ";
        printBitField(line, 1);*/

        /**
         * col setzt sich aus dem 1.ten bis 4.ten Bit zusammen
         */
        byte col[1] = {0};
        for (int x = 1; x < 5; x++) {
            setBit(col, 1, x + 3, getBit(zerlegt, 8, (i * 8) + x));
        } // for

        assert(line[0] >= 0 && line[0] < 4);
        assert(col[0] >= 0 && col[0] < 16);

        /**
         * anhand dem berechneten Spalten und Reihen Wert wird
         * der Wert aus der entsprechenden SBox Tabelle zurückgegeben.
         */
        sbox_result[i] = computeSBox(i, line[0], col[0]);

        //printBitField(sbox_result+i, 1, 4);
        //printBitField(sbox_result, 8);
    } // for

    /**
     * Aus den acht 4-bit Wörtern von den SBoxen werden vier 8-bit Wörter gemacht.
     */
    byte sbox_result_8[4] = {0};
    for (int i = 0; i < 32; i++) {
        setBit(sbox_result_8, 4, i, getBit(sbox_result, 8, i + 4 + 4 * (i / 4)));
    } // for

    /*cout << "S-Boxen      ";
    printBitField(sbox_result_8, 4);*/

    /**
     * Wende die Permutationstabelle P auf die 32 bits aus der SBox an.
     */
    permutate(pp, 32, sbox_result_8, 4, r_out, 4);

    /*cout << "f(r" << rnd << ", k" << rnd+1 << ")  = ";
    printBitField(r_out, 4);*/

} // functionF()


// #getBit()
bool DESCipher::getBit(const byte *array, int array_len, int pos) const {
    int bytepos, bitpos;
    byte b;

    assert(array_len > 0);
    assert(pos >= 0);

    /**
     * Die Byte Position und Bit Position werden im Array bestimmt.
     * Ein Byte wird entsprechend vorbereitet indem es um die entsprechende Bit Position nach Links geshiftet wird.
     * Anschließend wird der Returnwert mit einer geschickten "and" Operation erzeugt.
     */
    bytepos = pos / 8;
    bitpos = 7 - (pos % 8);
    if (bytepos < array_len) {
        b = 0x01;
        b = b << bitpos;
        // out << "(" << dec << bytepos << "," << bitpos << "," << hex << (short)b << ") ";
        return ((array[bytepos] & b) == b);
    } // if
    else {
        return false;
    } // else

} // getBit()


// #permutate()
void DESCipher::permutate
        (
                const byte *p,
                int p_len,
                const byte *in_array,
                int in_len,
                byte *out_array,
                int out_len
        ) const {
    // Tabelle p wird benutzt, um in_array in out_array zu permutieren
    /**
     * Permutationstabelle wird durchlaufen
     */
    for (int i = 0; i < p_len; i++) {
        /**
         * Das i-te Bit von out_array ist gleich dem (p[i]−1)-ten Bit von in_array
         */
        setBit(out_array, out_len, i, getBit(in_array, in_len, p[i] - 1));
    } // for
} // permutate()


// #printBitField()
void DESCipher::printBitField(const byte *bytefield, int len, int block_len) const {

    // Zählt die Länge des aktuell verarbeiteten Blocks
    // Mit dieser Variable wird detektiert, ob die Blocklänge erreicht ist
    int block_len_count = 0;
    /**
     * Iteriere durch jedes Bit im bytefield
     */
    for (int i = 0; i < len * block_len; i++) {
        /**
         * Gebe das Bit an Position i aus.
         */
        cout << getBit(bytefield, len, i);
        /**
         * Wenn Blocklänge erreicht -> Leerzeichen
         */
        if ((i + 1) % block_len == 0) {
            cout << " ";
        } // if
    } // for

    cout << endl;
} // printBitField()


// #processBlock()
void DESCipher::processBlock(const byte *in_block, byte *out_block) {
    /**
     * IP anwenden, dann Feistel Chiffre anwenden (16 Runden lang) und IP^(-1) anwenden.
     */

    /**
     * Die IP Permutationstabelle wird angewendet
     */
    //und der Block wird vorher und nach dem Anwenden ausgegeben.
    /*cout << "current input block:" << endl;
    printBitField(in_block, 8);*/
    permutate(ip, 64, in_block, 8, out_block, 8);
    /*cout << "after IP permutation:" << endl;
    printBitField(out_block, 8);
    cout << endl;*/

    /**
     * Der Block wird in zwei 4 byte Blöcke geteilt.
     */
    byte l_in[4];
    byte r_in[4];

    for (int i = 0; i < 4; i++) {
        l_in[i] = out_block[i];
        r_in[i] = out_block[i + 4];
    } // for

    byte l_out[4];
    byte r_out[4];

    /**
     * Es wird durch 16 Runden iteriert und die Feistel Chiffre angewendet.
     */
    for (int round = 0; round < 16; round++) {

        /*cout << "l" << round << "         = ";
        printBitField(l_in, 4);
        cout << "r" << round << "         = ";
        printBitField(r_in, 4);*/


        /**
         * Die Feistel chiffre wird angewendet
         */
        feistel(l_in, r_in, key_schedule[round], l_out, r_out, round);

        /**
         * Die Ausgaben werden die Eingaben für die nächste Runde von Feistel.
         */
        for (int i = 0; i < 4; i++) {
            l_in[i] = l_out[i];
            r_in[i] = r_out[i];
        } // for
        /*if(round+2 > 16) {
            cout << "r" << round+1 << " = ";
        } // if
        else {
            cout << "l" << round+2 << " = r" << round+1 << " = ";
        } // else
        printBitField(r_out, 4);
        cout << endl;*/
    } // for

    /*cout << "l16 = ";
    printBitField(l_out, 4);
    cout << "r16 = ";
    printBitField(r_out, 4);*/

    /**
     * Beim Zusammenkopieren der beiden Hälften Rechts und Links
     * wird Rechts und Links getauscht.
     */
    byte together[8] = {0};
    for (int i = 0; i < 4; i++) {
        together[i + 4] = l_in[i];
        together[i] = r_in[i];
    } // for

    /*cout << "together = ";
    printBitField(together, 8);
    cout << endl;*/

    /**
     * Die finale Permutationstabelle fp wird auf das Ergebnis angewendet.
     * Das Ergebnis steht nun in out_block.
     */
    permutate(fp, 64, together, 8, out_block, 8);

} // processBlock()


// #setBit()
void DESCipher::setBit
        (
                byte *array,
                int array_len,
                int pos,
                bool value
        ) const {
    int bytepos, bitpos;
    byte b;

    assert(array_len > 0);
    assert(pos >= 0);
    assert(pos < 8 * array_len);

    /**
     * Die Byte Position und Bit Position werden im Array bestimmt.
     * Ein Byte wird entsprechend vorbereitet indem es um die entsprechende Bit Position nach Links geshiftet wird.
     */
    bytepos = pos / 8;
    bitpos = 7 - (pos % 8);
    b = 0x01;
    b = b << bitpos;
    //cout << "(" << dec << bytepos << "," << bitpos << "," << hex << (short)b << ") ";
    /**
     * Beim Setzen des Bits wird das Byte im Array mit dem vorbereiteten Byte or durchgeführt.
     * Beim Löschen des Bits wird das Byte im Array mit 0xff xor durchgeführt und anschließend mit dem vorbereiteten Byte and durchgeführt.
     */
    if (value == true) {
        array[bytepos] |= b;
    } // if
    else {
        b = b ^ 0xff;
        array[bytepos] &= b;
    } // else

} // setBit()


// #aufgabe3test()
void DESCipher::aufgabe3test() {
    byte byteArr1[2] = {0}; // = { 47, 107 }
    byte byteArr2[2] = {0}; // = { 243, 192 };

    //0010111101101011
    setBit(byteArr1, 2, 0, 0);
    setBit(byteArr1, 2, 1, 0);
    setBit(byteArr1, 2, 2, 1);
    setBit(byteArr1, 2, 3, 0);
    setBit(byteArr1, 2, 4, 1);
    setBit(byteArr1, 2, 5, 1);
    setBit(byteArr1, 2, 6, 1);
    setBit(byteArr1, 2, 7, 1);
    setBit(byteArr1, 2, 8, 0);
    setBit(byteArr1, 2, 9, 1);
    setBit(byteArr1, 2, 10, 1);
    setBit(byteArr1, 2, 11, 0);
    setBit(byteArr1, 2, 12, 1);
    setBit(byteArr1, 2, 13, 0);
    setBit(byteArr1, 2, 14, 1);
    setBit(byteArr1, 2, 15, 1);

    //1111001111000000
    setBit(byteArr2, 2, 0, 1);
    setBit(byteArr2, 2, 1, 1);
    setBit(byteArr2, 2, 2, 1);
    setBit(byteArr2, 2, 3, 1);
    setBit(byteArr2, 2, 4, 0);
    setBit(byteArr2, 2, 5, 0);
    setBit(byteArr2, 2, 6, 1);
    setBit(byteArr2, 2, 7, 1);
    setBit(byteArr2, 2, 8, 1);
    setBit(byteArr2, 2, 9, 1);
    setBit(byteArr2, 2, 10, 0);
    setBit(byteArr2, 2, 11, 0);
    setBit(byteArr2, 2, 12, 0);
    setBit(byteArr2, 2, 13, 0);
    setBit(byteArr2, 2, 14, 0);
    setBit(byteArr2, 2, 15, 0);

    printBitField(byteArr1, 2, 8);
    printBitField(byteArr2, 2, 8);
} // aufgabe3test


// #ip
byte DESCipher::ip[64] = {
        58, 50, 42, 34, 26, 18, 10, 2,
        60, 52, 44, 36, 28, 20, 12, 4,
        62, 54, 46, 38, 30, 22, 14, 6,
        64, 56, 48, 40, 32, 24, 16, 8,
        57, 49, 41, 33, 25, 17, 9, 1,
        59, 51, 43, 35, 27, 19, 11, 3,
        61, 53, 45, 37, 29, 21, 13, 5,
        63, 55, 47, 39, 31, 23, 15, 7
};


// #fp
byte DESCipher::fp[64] = {
        40, 8, 48, 16, 56, 24, 64, 32,
        39, 7, 47, 15, 55, 23, 63, 31,
        38, 6, 46, 14, 54, 22, 62, 30,
        37, 5, 45, 13, 53, 21, 61, 29,
        36, 4, 44, 12, 52, 20, 60, 28,
        35, 3, 43, 11, 51, 19, 59, 27,
        34, 2, 42, 10, 50, 18, 58, 26,
        33, 1, 41, 9, 49, 17, 57, 25
};


// #ev
byte DESCipher::ev[48] = {
        32, 1, 2, 3, 4, 5,
        4, 5, 6, 7, 8, 9,
        8, 9, 10, 11, 12, 13,
        12, 13, 14, 15, 16, 17,
        16, 17, 18, 19, 20, 21,
        20, 21, 22, 23, 24, 25,
        24, 25, 26, 27, 28, 29,
        28, 29, 30, 31, 32, 1
};


// #pc1
byte DESCipher::pc1[] = {
        57, 49, 41, 33, 25, 17, 9,
        1, 58, 50, 42, 34, 26, 18,
        10, 2, 59, 51, 43, 35, 27,
        19, 11, 3, 60, 52, 44, 36,

        63, 55, 47, 39, 31, 23, 15,
        7, 62, 54, 46, 38, 30, 22,
        14, 6, 61, 53, 45, 37, 29,
        21, 13, 5, 28, 20, 12, 4
};

// #pc2
byte DESCipher::pc2[] = {
        14, 17, 11, 24, 1, 5,
        3, 28, 15, 6, 21, 10,
        23, 19, 12, 4, 26, 8,
        16, 7, 27, 20, 13, 2,
        41, 52, 31, 37, 47, 55,
        30, 40, 51, 45, 33, 48,
        44, 49, 39, 56, 34, 53,
        46, 42, 50, 36, 29, 32
};

// #sbox
byte DESCipher::sbox[8][64] = {
        /* S1 */
        14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7,
        0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8,
        4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0,
        15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13,

        /* S2 */
        15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10,
        3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5,
        0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15,
        13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9,

        /* S3 */
        10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8,
        13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1,
        13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7,
        1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12,

        /* S4 */
        7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15,
        13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9,
        10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4,
        3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14,

        /* S5 */
        2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9,
        14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6,
        4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14,
        11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3,

        /* S6 */
        12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11,
        10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8,
        9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6,
        4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13,

        /* S7 */
        4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1,
        13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6,
        1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2,
        6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12,

        /* S8 */
        13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7,
        1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2,
        7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8,
        2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11
};


// #pp
byte DESCipher::pp[32] = {
        16, 7, 20, 21,
        29, 12, 28, 17,
        1, 15, 23, 26,
        5, 18, 31, 10,
        2, 8, 24, 14,
        32, 27, 3, 9,
        19, 13, 30, 6,
        22, 11, 4, 25
};


// #total_rot
byte DESCipher::total_rot[16] = {
        1, 2, 4, 6, 8, 10, 12, 14, 15, 17, 19, 21, 23, 25, 27, 28
};
