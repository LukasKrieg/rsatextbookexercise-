/**
 * \file kasiski.cpp
 * \brief Das Konsolenprogramm, um den Kasisiki Test aus VigenereBreaker.cpp auszuführen.
 *
 * Kurz: Sucht in einer Geheimtext-Datei nach Trigrammen, Tetragrammen usw. und berechnet
 * anhand der Abstände, mit welche Schlüssellänge der Klartext verschlüsselt worden seien könnte.
 *
 * Trigramme sind Wörter der Länge 3. Tetragramme sind Wörter Länge 4. usw.
 * Die n-grame müssen jeweils mindestens 3-mal Vorkommen.
 */

#include <cassert>
#include <cstdlib>
#include <ctype.h>
#include <fstream>
#include <iostream>
#include "VigenereBreaker.h"

using namespace std;

/**
 * Liest die Geheimtext Datei ein und validiert die Kommandozeilen Argumente.
 * Zudem wird bei validen Kommandozeilenargumenten der Kasiski Test aus VigenereBreaker.cpp gestartet.
 *
 * @param argc (argument count): Die Anzahl der Kommandozeilen Argumente auf die argv zeigt
 * @param argv (argument vector): Das Array mit allen Kommandozeilen Argumenten (inklusive Programmaufruf)
 * @return Fehlercode
 * @retval 0 kein Fehler ist aufgetreten
 * @retval 1 Ein Fehler ist aufgetreten
 */
int main(int argc, char **argv) {
  VigenereBreaker vbreaker;
  ifstream in_file;
  vector<byte> key;
  vector<byte> cipher_text;
  int ngram_len;

  /*
   * Check whether at least one argument is given.
   */
  if (argc<=1) {
    cerr << "Usage: " << argv[0] 
         << " <in_file> [n-gram length]"
         << endl;
    exit(1);
  } // if

  /*
   * Read n-gram length 
   */
  ngram_len=0;
  if (argc>=3) {
    ngram_len=atoi(argv[2]); //atoi konvertiert/interpretiert einen String zum Int
  } // if
  if (ngram_len<=3) {
    ngram_len=3;
  } // if

  /* 
   * Read input file.
   */
  in_file.open(argv[1]);
  if (in_file.is_open()==false) {
    cerr << "Could not open file >" << argv[1] << "<" << endl;
    exit(1);
  } // if
  else {
    vbreaker.readStream(in_file, cipher_text);
    in_file.close();
  } // else

  // Analyse the cipher text
  vbreaker.kasiskiTest(cipher_text, ngram_len);
  
  return 0; 

} // main()
