#include <cassert>
#include "VigenereCipher.h"

// #decrypt()
int VigenereCipher::decrypt
(
 const vector<byte>& cipher_text, 
 const vector<byte>&  key,
 vector<byte>& plain_text)
{
    // cout << "cipher_text validieren (nur a-z erlaubt)" << endl;
    /**
     * Der cipher_text wird validiert (nur Werte zwischen [0, 25] erlaubt)
     */
    vector<byte>::const_iterator iter;
    for (iter = cipher_text.begin(); iter != cipher_text.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: cipher_text invalid" << endl;
            return 0;
        }
    }


    //cout << "Schlüssel validieren (nur a-z erlaubt)" << endl;
    /**
     * Der Schlüssel wird validiert:
     * - mindestens 1 Buchstabe
     * - nur Werte zischen [0, 25] erlaubt
     */
    if(key.size() <= 0) {
        cout << "ERROR: Schlüssel invalid: too short" << endl;
        return 0;
    }
    for (iter = key.begin(); iter != key.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: Schlüssel invalid " << endl;
            return 0;
        }
    }

    /*
     * Rotiert den Klartext, um die jeweiligen Schlüsselwerte.
     * Der Schlüssel wird dabei zyklisch verwendet.
     *
     * Siehe doxygen für ein detailiertes Beispiel
     */
    int i = 0; // zählt wieviele Buchstaben entschlüsselt wurden
    for (iter = cipher_text.begin(); iter != cipher_text.end(); ++iter) {
        int result = (*iter - key[i % key.size()]) % 26;
        if(result < 0) result += 26;
        plain_text.push_back(result);
        i++;
    }

    return i;
} // decrypt()


// #encrypt()
int VigenereCipher::encrypt
(
 const vector<byte>& plain_text,
 const vector<byte>& key, 
 vector<byte>& cipher_text
)
{
    //cout << "Klartext validieren (nur a-z erlaubt)" << endl;
    /**
     * Der Klartext wird validiert (nur Werte zwischen [0, 25] erlaubt)
     */
    vector<byte>::const_iterator iter;
    for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: Klartext invalid" << endl;
            return 0;
        } // if
    } // for


    //cout << "Schlüssel validieren (nur a-z erlaubt)" << endl;
    /**
     * Der Schlüssel wird validiert:
     * - mindestens 1 Buchstabe
     * - nur Werte zischen [0, 25] erlaubt
     */
    if(key.size() <= 0) {
        cout << "ERROR: Schlüssel invalid: too short" << endl;
        return 0;
    } // if
    for (iter = key.begin(); iter != key.end(); ++iter) {
        if(!(*iter >= 0 && *iter <= 25)) {
            cout << "ERROR: Schlüssel invalid " << endl;
            return 0;
        }
    } // for

    /*
     * Rotiert den Klartext, um die jeweiligen Schlüsselwerte.
     * Der Schlüssel wird dabei zyklisch verwendet.
     *
     * Siehe doxygen für ein detailiertes Beispiel
     */
    int i = 0; // zählt wieviele Buchstaben verschlüsselt wurden
    for (iter = plain_text.begin(); iter != plain_text.end(); ++iter) {
        cipher_text.push_back((*iter + key[i % key.size()]) % 26);
        i++;
    } // for

    return i;
} // encrypt()


