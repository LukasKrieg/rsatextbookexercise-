#ifndef _VigenereCipher_h_
#define _VigenereCipher_h_

#include "ClassicCipher.h"

/**
 * @class A class for the affine cipher.
 * @author Christoph Karg
 *
 * A class for the  Vigenere  cipher.
 */
class VigenereCipher : public ClassicCipher {
public:
  /**
   * Decrypt a cipher text.
   *
   * We choose a cipher_text and a key with values between [0, 26]
   *
   * To decrypt the cipher_text we rotate the cipher_text values back with the key values. (see example)
   *
   * Example:\n
   * <pre>
   * | m  | w  | z  | v  | b  | x  | i  | s  | v  | y  | w  | y  | w  | f  | x  | cipher text\n
   * | 12 | 22 | 25 | 21 | 1  | 23 | 8  | 18 | 21 | 24 | 22 | 24 | 22 | 5  | 23 | a (cipher text in int)\n
   * | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | b (key: "rot" or "17, 14, 19")\n
   * |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|\n
   * | 21 | 8  | 6  | 4  | 13 | 4  | 17 | 4  | 2  | 7  | 8  | 5  | 5  | 17 | 4  | ((a-b)+26) % 26 (plaintext)\n
   * | v  | i  | g  | e  | n  | e  | r  | e  | c  | h  | i  | f  | f  | r  | e  | (plaintext in char)\n
   * </pre>
   *
   * @param cipher_text array to store cipher text
   * @param cipher_len length of the cipher text array
   * @param key key
   * @param key_len key length
   * @param plain_text plain text to be decrypted
   * @param plain_len length of the plain text
   *
   * @return number of decrypted bytes
   */
  virtual int decrypt(const vector<byte>& cipher_text, 
                      const vector<byte>&  key,
                      vector<byte>& plain_text);

  /**
   * Encrypt a plain text.
   *
   * We choose a plain_text, a key with values between [0, 26].
   * To encrypt the plain_text we rotate the plain_text values with the key values. (see example)
   *
   * Example:
   * <pre>
   * | v  | i  | g  | e  | n  | e  | r  | e  | c  | h  | i  | f  | f  | r  | e  | clear text\n
   * | 21 | 8  | 6  | 4  | 13 | 4  | 17 | 4  | 2  | 7  | 8  | 5  | 5  | 17 | 4  | a (clear text in int)\n
   * | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | 17 | 14 | 19 | b (key: "rot" or "17, 14, 19")\n
   * |----|----|----|----|----|----|----|----|----|----|----|----|----|----|----|\n
   * | 12 | 22 | 25 | 21 | 1  | 23 | 8  | 18 | 21 | 24 | 22 | 24 | 22 | 5  | 23 | (a+b) % 26 (ciphertext)\n
   * | m  | w  | z  | v  | b  | x  | i  | s  | v  | y  | w  | y  | w  | f  | x  | (ciphertext in char)\n
   * </pre>
   * @param plain_text plain text to be encrypted
   * @param plain_len length of the plain text
   * @param key key
   * @param key_len key length
   * @param cipher_text array to store cipher text
   * @param cipher_len length of the cipher text array
   *
   * @return number of encrypted bytes
   */
  virtual int encrypt( const vector<byte>& plain_text,
                       const vector<byte>& key, 
                       vector<byte>& cipher_text);

}; // VigenereCipher

#endif
