#include <cassert>
#include <map>
#include <iomanip>
#include <set>
#include <string>
#include <vector>
#include "VigenereBreaker.h"


// #analyse()
void VigenereBreaker::analyse(const vector<byte>& cipher_text) {
  int kasiski_len;

  // Check length of cipher text
  if (cipher_text.size()==0) return;

  // Perform Kasiski test
  kasiski_len = kasiskiTest(cipher_text);

  cout << "Kasiski test: " << kasiski_len << endl;

} // analyse()


// #gcd()
int VigenereBreaker::gcd(int a, int b) const {
  int r;

  do  {
    r = a % b;
    a = b;
    b = r;
  } while (r!=0);

  return a;
    
} // gcd()


// #gcd()
int VigenereBreaker::gcd(const vector<int>& v) const {
  int r,i;

  // Vektor darf nicht leer sein.
  assert(v.size()>0);

  // Wenn der Vektor die Länge 1 hat, dann wird die eine Zahl zurückgegeben.
  if (v.size()==1) {
    return v[0];
  } // if
  else {

    /**
     * Der gcd von den ersten beiden Zahlen aus dem Vector wird berechnet.
     * Anschließend wird der gcd mit dem Ergebnis und der nächsten Zahl im Vektor berechnet.
     * Den letzten Schritt wiederholt man solange bis der Vector vollständig durchlaufen ist.
     */
    r = gcd(v[0], v[1]);
    i = 2;
    while ((r>1) && (i<v.size())) {
      r = gcd(r, v[i]);
      i++;
    } // while

    return r;

  } // else

} // gcd()


// #kasiski()
int VigenereBreaker::kasiskiTest
(
 const vector<byte>& cipher_text,
 int ngram_len
) 
{

    // Konvertiere cipher_text Vector zu String
    string cipher_string(cipher_text.begin(), cipher_text.end());
    // Beinhaltet die Buchstabensequenzen und deren Positionen im Geheimtext
    map<string, vector<int> > result_table;
    // Variable, um sich die Positionen der Buchstabensequenzen zu merken
    vector<int> ngram_positions;
    // Temporäre Variable, um zu zählen wie viele Buchstaben in einer Sequenz aktuell übereinstimmen zum gesuchten Wort
    int count_sequential_match;
    // Speichert wie oft ein gcd vorkommt
    map<int, int> statistic_table; // <gcd, count>
    // Temporäre Variable, welches das Ergebnis einer gcd Berechnung beinhaltet
    int tmpgcd;

    /*
     * Iteriere durch den Geheimtext und suche nach n-Grammen.
     * Beispiel mit ngram_len = 3:
     *   - Jede dreier Sequenz von Buchstaben wird als string extrahiert.
     *   - Anschließend wird diese Sequenz im Geheimtext gesucht.
     *   - Die Positionen der Übereinstimmungen werden in einem Vector festgehalten.
     *   - Wenn es drei oder mehr Übereinstimmungen gab, dann wird der Abstand relativ zum ersten Vorkommen berechnet.
     *   - Der GCD von den Abständen wird berechnet.
     *   - Die Ergebnisse werden ausgegeben.
     */
    for (int i = 0; i < cipher_string.size()-ngram_len+1; i++)  {

        // Extrahiere das gesuchte Wort an Position i bis Position i+ngram_len
        string search_word = cipher_string.substr(i, ngram_len); // [i, i + ngram_len)
        // Zählt hoch, wenn Übereinstimmung mit search_word gefunden
        count_sequential_match = 0;
        // Setze die gefundenen Positionen vor jeder Runde zurück
        ngram_positions.clear();

        /**
         * Durchläuft den Geheimtext von i bis zum Ende. Die erste Übereinstimmungen ist immer direkt am Anfang der Schleife,
         * da die Schleife bei diesem Wort beginnt. Anschließend werden weitere n-Gramme gesucht, die mit dem gesuchten Wort
         * übereinstimmen.
         */
        for(int n = i; n < cipher_string.size()-ngram_len+1; n++) {
            /**
             * Schaut um maximal n-Gramm Schritte nach vorne, um nach Übereinstimmungen zu schauen mit
             * dem gesuchten Wort.
             */
            for(int j = n; j < n + ngram_len; j++) {
                /**
                 * Stimmt der aktuelle Buchstabe mit dem Substring überein, dann wird
                 * im nächsten Schleifendurchlauf, der nächste Buchstabe zwischen Substring und Geheimtext
                 * auf Übereinstimmung überprüft, bis die Anzahl der Übereinstimmungen in einer Sequenz die n-Gramm
                 * Länge erreicht haben. Daraufhin wird die gefundene Übereinstimmung dem Results-Vector hinzugefügt.
                 */
                if((int)search_word[count_sequential_match] == (int)cipher_string[j]) {
                    count_sequential_match++;
                    if(count_sequential_match == ngram_len) {
                        ngram_positions.push_back(n);
                        count_sequential_match = 0;
                        break;
                    } // if
                } else {
                    count_sequential_match = 0;
                    break;
                } // if
            } // for
        } // for

        // add every ngramm (at least 3 matches)
        /**
         * Jedes n-Gramm, welches 3 oder mehr Übereinstimmungen im Geheimtext hat, wird dem Result hinzugefügt
         */
        if(ngram_positions.size() > 2) {
            result_table.insert(pair<string, vector<int> >(search_word, ngram_positions));
        } // if
    }


    // Speichert temporär die Abstände zum ersten Vorkommen.
    vector<int> all_diffs;
    // Speichert temporär einzelne Abstände zwischen Vorkommen zu erstem Vorkommen
    int diff;
    /**
     * Zu jedem Wort, welches mindestens 3-mal im Geheimtext vorkommt, wird der Abstand zum ersten Wort berechnet.
     */
    for(map<string, vector<int> >::iterator map_it = result_table.begin(); map_it != result_table.end(); map_it++) {
        //cout << "check trigramm: " << map_it->first << endl;
        all_diffs.clear(); // Vor jedem Lauf zurücksetzen.

        /**
         * Jeden Abstand zum ersten Vorkommen berechnen.
         */
        for(int i = 1; i < map_it->second.size(); i++) {
            diff = map_it->second[i] - map_it->second[0];
            all_diffs.push_back(diff);
        } // for

        // GCD berechnen
        tmpgcd = gcd(all_diffs);

        /**
         * Beispielausgabe:
         *
         * Wort: ...
         * Positionen im Geheimtext: ... ... ...
         * Abstände zum ersten Vorkommen: ... ...
         * GCD(..., ...) = ...
         */
        cout << "Wort: ";
        for(int i = 0; i < map_it->first.size(); i++) {
            cout << (char)(((int)map_it->first[i])+65);
        } // for
        cout << endl;
        cout << "Positionen im Geheimtext: ";
        for(int i = 0; i < map_it->second.size(); i++) {
            cout << map_it->second[i] << " ";
        } // for
        cout << endl;
        cout << "Abstände zum ersten Vorkommen: ";
        for(int i = 0; i < all_diffs.size(); i++) {
            cout << all_diffs[i] << " ";
        } // for
        cout << endl;
        cout << "GCD(";
        for(int i = 0; i < all_diffs.size()-1; i++) {
            cout << all_diffs[i] << ", ";
        } // for

        statistic_table[tmpgcd]++;

        cout << all_diffs[all_diffs.size()-1] << ") = " << tmpgcd << endl;
        cout << endl;
    } // for



    // Speichere den GCD, welcher am öftesten vorkommt.
    int curBestGCD = 0;
    int curBestCount = -1;
    /**
     * Beispielausgabe:
     *
     * Statistik:
     * GCD: 1 Count: 4
     * GCD: 2 Count: 3
     * ....
     *
     * zusätzlich finde den GCD mit den
     */
    cout << "Statistik:" << endl;
    for(map<int, int>::iterator map_it = statistic_table.begin(); map_it != statistic_table.end(); map_it++) {
        cout << "GCD: " << map_it->first << " Count: " << map_it->second << endl;
        if(map_it->second > curBestCount) {
            curBestGCD = map_it->first;
            curBestCount = map_it->second;
        }
    }

    /**
     * Gebe die vermutete Schüssellänge aus.
     */
    cout << endl << "Die vermutete Schlüssellänge ist: " << curBestGCD << endl;

    return curBestGCD;
} // kasiski()

// #coincidenceIndex()
float VigenereBreaker::coincidenceIndex(const vector<byte>& text) {
    // Summe aller Buchstaben
    float result = 0;
    /**
     * Iteriert durch alle Buchstaben 0-25.
     */
    for(int i = 0; i < 26; i++) {
        int count = 0;
        /**
         * Iteriert durch den Text und zählt, wie oft der Buchstabe vorkommt.
         */
        for(int n = 0; n < text.size(); n++) {
            if(text[n] == i) {
                count++;
            }
        }
        // Summiert die Buchstabenhäufigkeit nach Formel. (siehe Doxygen)
        result += count * (count-1);
    }
    // Dividiert durch die Gesamthäufigkeit nach Formel. (siehe Doxygen)
    result /= (text.size()*(text.size()-1));
    return result;
} // coincidenceIndex()


// #coincidenceTest()
bool VigenereBreaker::coincidenceTest
(
 const vector<byte>& cipher_text,
 int cols, 
 float threshold
) 
{
    // Teile den cipher_text in cols Spalten auf.
    // Wenn wir die richtige Schlüssellänge erwischen, müsste die Buchstabenhäufigkeit der englischen Sprache enstprechen.
    // Anderfalls ist die Buchstabenhäufigkeit eher zufällig
    // m ist die geratene Schlüssellänge

    // Eine temporäres Array, welches die Ergebnisse aus Berechnung des Koinzidenzindex beinhaltet
    vector<float> results;
    // Im Minimum wird für die Schlüssellänge/Spaltengröße eins die Rechnung durchgeführt.
    if(cols >= 1) {
        //beinhaltet den geteilten Text.
        vector<byte> part;

        /**
         * Teilt den cipher_text in cols Spalten auf und berechnet jeweils
         * den Koinzidenzindex,
         *
         * Schleife von 0 .. cols
         */
        for(int i = 0; i < cols; i++) {
            part.clear();
            // Starte bei i und Springe jeweils cols Schritte
            for(int n = i; n < cipher_text.size(); n+=cols) {
                part.push_back(cipher_text[n]);
            }
            // Für den geteilten Text jeweils den Koinzidenzindex berechnen
            results.push_back(coincidenceIndex(part));
        }

        // Tabellen Formatierung
        cout << cols << "         ";

        // Hilfsvariable, um den Return Wert zu bestimmen.
        bool thresh_ok = true;

        /**
         * Iteriert durch die Resulte gibt diese aus
         * und schaut, ob die Resultate jeweils den Threshold überschreiten.
         */
        for(int i = 0; i < results.size(); i++) {
            cout << results[i] << " ";
            if(results[i] <= threshold) {
                thresh_ok = false;
            } // if
        } // for

        // Wenn alle Ergebnisse einer Spalten Aufteilung den Schwellwert überschreiten, dann
        // wird diese Zeile gesondert markiert.
        if(thresh_ok) {
            cout << ">> POSSIBLE MATCH: key length: " << cols << " <<" << endl;
            return true;
        } // if
        cout << endl;
    } // if
    else {
        cout << "Error: Invalid cols/key" << endl;
    } // else

    return false;
} // coincidenceTest()


// #mutualCoinIndex()
int VigenereBreaker::mutualCoinIndex
(
 const vector<byte>& cipher_text,
 int cols,
 int col_i,
 int col_j, 
 float threshold) 
{
    // Geratene Schlüssellänge/Spaltengröße muss mindestens eins sein.
    if(cols >= 1) {
        // Der cipher_text wird in Zwei Teile aufgeteilt partI und partJ
        vector<byte> partI; // Text A
        vector<byte> partJ; // Text B

        // Das Resultat der Rechnung
        float result = 0;

        // Text A (partI) wird geleert
        partI.clear();
        /**
         * Iteriere in cols Schritten durch den cipher_text und füge die
         * Buchstaben Text A hinzu.
         */
        for (int n = col_i; n < cipher_text.size(); n += cols) {
            partI.push_back(cipher_text[n]);
        }

        // Text B (partJ) wird geleert
        partJ.clear();
        /**
         * Iteriere in cols Schritten durch den cipher_text und füge die
         * Buchstaben Text B hinzu.
         */
        for (int n = col_j; n < cipher_text.size(); n += cols) {
            partJ.push_back(cipher_text[n]);
        }

        // Zum Speichern des Shifts mit dem größten gegenseitigen Koinzidenzindex
        float curMaxIndex = 0; // größter gegenseitigen Koinzidenzindex
        int curBestShift = -1; // Shift, welcher auf das Ergebnis kommt

        // Buchstabenhäufigkeit in Text A und Text B  (+Teste jeden Shift von Text B)
        /**
         * Iteriere über alle Verschiebungen (Shifts)
         */
        for(int g = 0; g < 26; g++) { //shift
            result = 0;

            /**
             * Iteriere über alle 26 Buchstaben und zähle die Häufigkeit
             * der Buchstaben in Text A und Text B
             */
            for(int i = 0; i < 26; i++) {
                int countI = 0;
                int countJ = 0;

                /**
                 * Zähle die Häufigkeit des Buchstabens i in Text A
                 */
                for (int n = 0; n < partI.size(); n++) {
                    if (partI[n] == i) {
                        countI++;
                    } // if
                } // for

                /**
                 * Zähle die Häufigkeit des Buchstabens i in Text B
                 * Die Buchstabenhäufigkeit von Text B verschiebt sich um g
                 */
                for (int n = 0; n < partJ.size(); n++) {
                    if (partJ[n] == ((i+g)%26)) {
                        countJ++;
                    } // if
                } // for

                // Summiert die Buchstabenhäufigkeit nach Formel. (siehe Doxygen)
                result += countI * countJ;
            } // for

            // Dividiert durch die Gesamthäufigkeit nach Formel. (siehe Doxygen)
            result /= (partI.size()*partJ.size());

            // Sorge für korrekte Formatierung der Tabelle
            // Zahlen mit der Länge 1 kriegen ein Leerzeichen mehr Abstand, als Zahlen der Länge 2.
            if(g <= 9) {
                cout << "   ";
            } // if
            else {
                cout << "  ";
            } // else
            // Gebe den Shift inklusive dem berechneten gegenseitigem Koinzidenzindex aus
            // Runde auf die dritte Kommastelle
            printf("%d : %.3f", g, result);

            // Formatierung: Nach 7 Elementen kommt jeweils ein Zeilenumbruch
            if((g != 0) && ((g+1) % 7) == 0) {
                cout << endl << "   ";
            } // if

            // Der größte gegenseitige Koinzidenzindex wird für jede Verschiebungsrunde gesucht.
            if(result > curMaxIndex) {
                curMaxIndex = result;
                curBestShift = g;
            } // if
        } // for

        cout << endl; // Formatierung

        // Wenn der größte gegenseitige Koinzidenzindex größer gleich dem threshold ist,
        // dann gebe die beste Verschiebung zurück.
        if(curBestShift >= 0 && curMaxIndex >= threshold) {
            cout << "-> Shift " << curBestShift << " is greater or equal threshold, mutualCoinIndex: " << curMaxIndex;
            return curBestShift;
        } // if
        else {
            cout << "-> No Shift is greater or equal threshold.";
        } // else
    }

    return -1;
} // mutualCoinIndex()
