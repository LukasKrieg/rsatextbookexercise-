#ifndef VigenereBreaker_h_
#define VigenereBreaker_h_

#include <vector>
#include "VigenereCipher.h"

/**
 * @class A class containing analysis tools to break
 * the Vigenere cipher
 * @author Christoph Karg
 */
class VigenereBreaker : public VigenereCipher {
protected:

  /**
   * Compute greatest common divisor of two integers a and b.
   * Using the Euclidean algorithm.
   * 
   * @param a first integer
   * @param b second integer
   *
   * @return gcd(a,b)
   */
  int gcd(int a, int b) const;

  /** 
   * Compute the greatest common divisor of a vector of integers.
   *
   * The vector must be of size > 0.
   *
   * The algorithm will calculate the gcd over two Integers and
   * is reusing the result in the next gcd calculation until
   * the end of the vector is reached.
   *
   * @param v vector containing at least 1 integer
   *
   * @return greatest common divisor of all integers in v
   */
  int gcd(const vector<int>& v) const;

public:

  /**
   * Perform Kasiski test to detect key length.
   *
   * 1) convert cipher_text vector to string\n
   * 2) search n-grams and remind absolute position in map<string, vector<int> > variable\n
   * 3) for each n-gram which exists at least three times calculate the relative distance
   *    to the first occurence.\n
   * 4) Calculate gcd over all distances\n
   * 5) Output results\n
   *
   * @param cipher_text cipher text to be analysed
   * @param ngram_len length of the n-grams to be checked
   *
   * @return key length (0 = no length computed)
   */
  int kasiskiTest(const vector<byte>& cipher_text,
                  int ngram_len=3);

  /**
   * Compute the coincidence indices for a given text.
   *
   * coincidence indices: Probability of two randomly choosen chars from the text to be the same.
   *
   * formula: l_c(x) = (sum(f_i*(f_i - 1),i = 0 .. 25))/(n*(n - 1)) \n
   * ASCII ART:\n
   * <pre>
   *              25
   *             =====
   *             \
   *              >    f  /f  - 1\
   *             /      i \ i    /
   *             =====
   *             i = 0
   *     l (x) = -----------------
   *      c          n (n - 1)
   *
   * x = input text
   *
   * f  = the count of char i in x
   *  i
   *
   * n = count of all chars
   * </pre>
   *
   * Note: used aamath to generate ascii art math
   *
   * @param text text to be analysed
   *
   * @return coincidence index of text
   */
  float coincidenceIndex(const vector<byte>& text);

  /**
   * Compute the coincidence indices of a given number of
   * columns and check whether all values lie above a 
   * given threshold.
   *
   * Each key size will be tested until cols number
   * The text will be splitted in multiple parts (depending on cols count).
   *
   * Coincidence indices does have special values in every language.
   * e.g. english text: ~0.065
   *
   *
   * @param cipher_text cipher_text to be analysed
   * @param cols number of columns
   * @param threshold threshold value
   *
   * @return true <=> indices of all columns are > threshold
   *                  This method returns true when the indices of all columns are over the threshold
   *                  ("Die Methode soll genau dann true zurückliefern, wenn die Indizes aller Spalten über dem Schwellwert threshold liegen.")
   */
  bool coincidenceTest(const vector<byte>& cipher_text,
                       int cols, 
                       float threshold=0.065);

  /**
   * Compute the mutual coincidence index table of two columns
   * and return the relative shift g such that key[j] = key[i] - g
   * if the respective index is >= threshold
   *
   * mutual coincidence index: probability for two randomly choosen chars from text x and text y
   *                           to be the same
   *
   * formula: MI_c(x,y) = (sum(f_i^x*f_i^y,i = 0 .. 25))/(n_x*n_y) \n
   * ASCII ART:\n
   * <pre>
   *                    25
   *                   =====
   *                   \      x  y
   *                    >    f  f
   *                   /      i  i
   *                   =====
   *                   i = 0
   *       MI (x, y) = -----------
   *         c            n  n
   *                       x  y
   *
   * x, y = input text
   *
   *  x
   * f  = the count of char i in x
   *  i
   *
   *  y
   * f  = the count of char i in y
   *  i
   *
   * n  = count of all chars in x
   *  x
   *
   * n  = count of all chars in y
   *  y
   * </pre>
   *
   * Will print a table to stdout with i, j and all calculated mutual coincidence index.
   * Also a key matrix is populated with mutual coincidence index values exceeding the threshold.
   * Look for rows in key matrix where all values are >= 0 to get possible keys.
   *
   * @param cipher_text cipher_text to be analysed
   * @param cols number of columns
   * @param col_i first column
   * @param col_j second column
   * @param threshold thershold value
   *
   * @return shift g between key[i] and key[j], if respective index 
   * is >= threshold. -1, otherwise.
   */
  int mutualCoinIndex(const vector<byte>& cipher_text,
                      int cols, 
                      int i,
                      int j,
                      float threshold=0.065);

  /**
   * Analyse a given cipher text.
   * Not implemented.
   * @param cipher_text cipher text to be analysed
   */
  void analyse(const vector<byte>& cipher_text);

};

#endif
