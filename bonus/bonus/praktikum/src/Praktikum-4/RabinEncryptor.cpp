/*
 * RabinEncryptor.cpp
 */

#include "RabinEncryptor.h"
#include "PublicKeyAlgorithmBox.h"
#include <iostream>
#include <algebra.h>
#include <sstream>

RabinEncryptor::RabinEncryptor(const Integer& n, const Integer& padding) {
    RabinEncryptor::n = n; // öffentlicher Teil des Schlüssels
    RabinEncryptor::padding = padding; // Ein Padding zum späteren identifizieren des Klartextes beim Entschlüsseln
    /**
     * Die Padding Länge wird in offset geschrieben.
     * Dazu wird der Integer in einen String konvertiert und die Länge ausgelesen.
     */
    std::stringstream ss;
    ss << padding;
    RabinEncryptor::offset = ss.str().length();
}

RabinEncryptor::~RabinEncryptor() {
}

// #compute()
bool RabinEncryptor::compute(const Integer& x, Integer& y) {
    PublicKeyAlgorithmBox box;
    /**
     * Klartext muss in Zn liegen
     */
    if(x < 0 || x >= n) return false;
    /**
     * Klartext x wird mit x*x mod n verschlüsselt und das Ergebnis wird in y gespeichert.
     */
    y = box.modularExponentation(x, 2, n);
    return true;
}

// #compute2()
bool RabinEncryptor::compute2(const Integer& x, Integer& y) {
    /**
     * Um an x das padding zu hängen wird x mit 10^offset multipliziert.
     * Anschließend kann der padding aufaddiert werden.
     */
    Integer base = 10, exp = offset-1;
    Integer tmp = EuclideanDomainOf<Integer>().Exponentiate(base, exp);
    Integer xzero = x * tmp;
    Integer marked = xzero + padding;
    return compute(marked, y);
}
