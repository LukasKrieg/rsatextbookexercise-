/*
 * RSAAttack.cpp
 */

#include <vector>
#include <iostream>
#include <assert.h>
#include <iomanip>
#include <sstream>
#include "PublicKeyAlgorithmBox.h"
#include "RSAAttack.h"

using namespace std;

RSAAttack::RSAAttack() {
}

RSAAttack::~RSAAttack() {
}

// #factorizeN()
bool RSAAttack::factorizeN(const Integer &n, const Integer &phi_n, Integer &p,
                           Integer &q) const {
    PublicKeyAlgorithmBox box;
    /**
     * setze variablen für Mitternachtsformel
     */
    Integer a = 1;
    Integer b = phi_n-n-1;
    Integer c = n;
    Integer s;
    /**
     * berechne Wunrzel in Mitternachtsformel
     *
     */
    box.sqrt((b*b-4*a*c),s);

    /**
     * Berechene die beiden Ergebnisse der Mitternachtsformel
     */
    p=(-b+s)/(2*a);
    q=(-b-s)/(2*a);

    /**
     * Prüfe ob Ergebniss stimmen kann, p*q muss n sein.
     */
    if(p*q==n)
        return true;
    return false;
}

// #wienerAttack()
bool RSAAttack::wienerAttack(const Integer &n, const Integer &e, Integer &p,
                             Integer &q) const {
    /**
     * Konvergenten berechnen des Kettenbruchs e/n
     */
    PublicKeyAlgorithmBox box;
    vector<Integer> c;
    vector<Integer> d;

    box.computeConvergents(e, n, c, d);

    for(int i = 2; i < c.size(); i++) {
        /**
         * Prüfe ob das mögliche phi(n) eine ganze Zahl ist.
         */
        if(((e * d[i] - 1) % c[i]) != 0) {
            continue;
        }
        Integer phi_n = ((e * d[i] - 1) / c[i]);

        /**
         * Prüfe ob n anhand von phi(n) faktorisierbar ist
         */
        if(!factorizeN(n, phi_n, p, q)) {
            continue;
        }

        /**
         * p und q wurden gefunden.
         */
        return true;
    }

    return false;
}


// #halfAttack()
Integer RSAAttack::halfAttack(const Integer &n, const Integer &e, const Integer &y,
                              RSAOracle &rsa_oracle) const {
    Integer l, othery = y;
    vector<bool> h;
    PublicKeyAlgorithmBox box;
    l = n.BitCount();
    /**
     * Gehe die Bitlänge des Klartextraumes durch.
     */
    for(int i = 0; i < l; i++) {
        /**
         * Speichere das höchstwertigste Bit in dem vector h.
         */
        h.push_back(rsa_oracle.half(othery));
        /**
         * Es wird ein Linksshift um 1 gemacht indem y * (2^e mod n) gerechnet wird. Durch das Modulo wird das vorherige
         * höchstwertigste Bit abgeschnitten und so kann das nächste Bit ausgelesen werden.
         */
        othery = othery * box.modularExponentation(2, e, n);
    }
    /*for(int i = 0 ; i < l; i++) {
        cout << h[i] << endl;
    }*/

    /**
     * Ausgabe mit fester Spaltenbreite für die Ausgabe der Binären Suche, als Tabelle.
     */
    const char sep    = ' '; // seperator
    const int smallWidth = 5; // für fünfstellige Spalten
    const int bigIntWidth = 50; // für maximal 50 stellige BigInteger
    cout << left << setw(smallWidth) << setfill(sep) << "i";
    cout << left << setw(smallWidth) << setfill(sep) << "h[i]";
    cout << left << setw(bigIntWidth) << setfill(sep) << "lo";
    cout << left << setw(bigIntWidth) << setfill(sep) << "mid";
    cout << left << setw(bigIntWidth) << setfill(sep) << "hi";
    cout << endl;

    /**
     * Mache eine Binäre Suche mithilfe der Bits aus der half Funktion.
     * Da bei der Binären Suche immer durch 2 geteilt wird, können Kommazahlen vermieden werden,
     * indem im Vorhinein die obere Grenze so oft mal zwei gerechnet wird, wie
     * später geteilt durch 2 gerechnet wird. -> Integer::Power2(n.BitCount())
     */
    Integer factor = Integer::Power2(n.BitCount());
    //cout << "BITCOUNT: " << n.BitCount() << endl;
    Integer lo("0"), mid, hi = n*factor;
    Integer quotient, divisor, rest, tmp;
    string tmpkomma;
    int precision;
    stringstream ss;
    for(int i = 0; i < l; i++) {
        mid = (hi + lo) / 2;


        /**
         * Zunächst Nachkommastellen berechnen. Siehe hierzu PublicKeyAlgorithmBox::decimalDivision.
        */

        /**
         * Ausgabe der Binären Suche in Tabelle mit fester Spaltengrößen
         */
        PublicKeyAlgorithmBox box;
        cout << left << setw(smallWidth) << setfill(sep) << i;
        cout << left << setw(smallWidth) << setfill(sep) << h[i];
        cout << left << setw(bigIntWidth) << setfill(sep) << box.decimalDivison(lo, factor, bigIntWidth-2);
        cout << left << setw(bigIntWidth) << setfill(sep) << box.decimalDivison(mid, factor, bigIntWidth-2);
        cout << left << setw(bigIntWidth) << setfill(sep) << box.decimalDivison(hi, factor, bigIntWidth-2);
        cout << endl;

        if(h[i] == 1) {
            lo = mid;
        } else {
            hi = mid;
        }
    }
    return hi/factor;
}
