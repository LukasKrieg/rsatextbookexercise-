/*
 * RSAEncryptor.cpp
 */

#include "PublicKeyAlgorithmBox.h"
#include "RSAEncryptor.h"

RSAEncryptor::RSAEncryptor(const Integer &n, const Integer &e) {
    this->e = e;
    this->n = n;
}

RSAEncryptor::~RSAEncryptor() {}

bool RSAEncryptor::compute(const Integer &x, Integer &y) const {
    PublicKeyAlgorithmBox box;
    /**
     * Prüfen ob der Plaintext valide ist.
     */
    if(x>=n || x < 0)
        return false;
    /**
     * Verschlüsselung mit modularer Exponentation durchführen.
     */
    y = box.modularExponentation(x,e,n);
    return true;
}
