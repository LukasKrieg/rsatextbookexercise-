/*
 * RabinDecryptor.h
 */

#ifndef RABINDECRYPTOR_H_
#define RABINDECRYPTOR_H_

#include <vector>
#include "integer.h"

using namespace CryptoPP;
using namespace std;

class RabinDecryptor {
private:
	/** Der private Teil p des Schlüssels als BigInteger */
	Integer p;
	/** Der private Teil q des Schlüssels als BigInteger */
	Integer q;
	/** Ein optionaler Padding als BigInteger */
	Integer padding;
	/** Die Länge des Padding als BigInteger  */
	Integer offset;

public:
	/**
	 * Der Konstruktor von RabinDecryptor. Der private Schlüssel zum entschlüsseln und der padding wird
	 * mit entsprechender Länge zu den privaten Variablen im Objekt zugewiesen.
	 *
	 * @param p BigInteger Rabin Primzahl als Teil des privaten Schlüssels.
	 * @param q BigInteger Rabin Primzahl als Teil des privaten Schlüssels.
	 * @param padding Der optionale Padding, welcher im Klartext erkannt werden kann.
	 */
	RabinDecryptor(const Integer& p, const Integer& q, const Integer& padding=Integer("987654321"));
	virtual ~RabinDecryptor();

	/**
     * Beim Verschlüsseln wurde das Quadrat des Klartextes Modulo n gerechnet und y ist herausgekommen.
     * Um diese Operation Rückgängig zu machen muss die Quadratwurzel von y Modulo n aus dem Ciphertext gezogen worden.
     * Dies ist jedoch nicht trivial möglich, da n keine Primzahl ist.
     * Da p und q bekannt ist, kann die Quadratwurzel von y Modulo p und die Quadratwurzel von y Modulo q berechnet werden.
     * Dazu wird ein Satz bzw. eine Formel verwendet, welche zwei mögliche Lösungen für die Quadratwurzel ausgibt pro Rechnung.
     *
     * x1 und x2 sind die Lösungen der Gleichung: y^2 ≡ a (mod p)
     * y1 und y2 sind die Lösungen der Gleichung: y^2 ≡ a (mod q)
     *
     * Um nun die Ergebnisse der Wurzelrechnungen zu dem Ergebnis der Wurzeln von n zusammenzusetzen, wird mit dem
     * EEA die multiplikativen Inversen mp und mq berechnet.
     *
     * Zum Schluss kombiniert der chinesische Restsatz die Wurzeln.
     *
     * @param y Der Geheimtext als BigInteger
	 * @param xv Ein Vector der Größe 4, in welche die 4 möglichen Klartexte geschrieben werden.
	 * @return false im Fehlerfall, sonst true
     */
	bool compute(const Integer& y, vector<Integer>& xv);
	/**
	 * Ruft compute(const Integer& y, vector<Integer>& xv) auf und speichert das erste der vier Ergebnisse
	 * in x.
	 * @param y Der Geheimtext als BigInteger
	 * @param x Der Klartext als BigInteger
	 * @return false im Fehlerfall, sonst true
	 */
	bool compute(const Integer& y, Integer& x);

	/**
	 * Ruft compute(const Integer& y, vector<Integer>& xv) auf und detektiert in den Vier möglichen Ausgaben das
	 * Padding.
	 * @param y Der Geheimtext als BigInteger
	 * @param x Der Klartext als BigInteger
	 * @return false im Fehlerfall, sonst true
	 */
	bool compute2(const Integer& y, Integer& x);
};

#endif /* RABINSYSTEM_H_ */
