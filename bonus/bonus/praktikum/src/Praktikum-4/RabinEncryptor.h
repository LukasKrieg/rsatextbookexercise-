/*
 * RabinEncryptor.h
 */

#ifndef RABINENCRYPTOR_H_
#define RABINENCRYPTOR_H_

#include "integer.h"

using namespace CryptoPP;

class RabinEncryptor {
private:
	/** Der öffentliche Schlüssel n als BigInteger */
	Integer n;
	/** Die Länge des Paddings als BigInteger */
	Integer offset;
	/** Das Padding als BigInteger */
	Integer padding;

public:
	/**
	 * Konstruktor des RabinEncryptors.
	 * Bekommt den öffentlichen Teil des Schlüssels, um den Klartext verschlüsseln zu können. Ein Padding kann beim
	 * verschlüsseln optional verwendet werden, um den Klartext später wiederzuerkennen. Die Parameter werden zu
	 * den entsprechenden privaten Variablen im Objekt zugewiesen.
	 *
	 * @param n Der öffentliche Teil des Schlüssels
	 * @param padding Ein Padding zum optionalen markieren des Klartextes
	 */
	RabinEncryptor(const Integer& n, const Integer& padding=Integer("987654321"));
	virtual ~RabinEncryptor();

	/**
	 * Verschlüsselt den Klartext x mit dem quadratischen Rest und schreibt das Ergebnis in y.\n
	 * Formel: y = (x * x) % n
	 *
	 * @param x Der Klartext als BigInteger
	 * @param y Der verschlüsselte Klartext wird in y geschrieben
	 * @return false, wenn x in Klartextraum passt (0 <= x < n), sonst true
	 */
	bool compute(const Integer& x, Integer& y);

	/**
	 * Ein Padding wird an x angehängt bevor der x mit compute verschlüsselt wird.
	 *
	 * @param x Der Klartext als BigInteger
	 * @param y Der verschlüsselte Klartext wird in y geschrieben
	 * @return false, wenn x in Klartextraum passt (0 <= x < n), sonst true
	 */
	bool compute2(const Integer& x, Integer& y);
};

#endif /* RABINENCRYPTOR_H_ */
