/*
 * RSADecryptor.cpp
 */

#include "PublicKeyAlgorithmBox.h"
#include "RSADecryptor.h"

#include <iostream>

using namespace std;

RSADecryptor::RSADecryptor(const Integer& p, const Integer& q,
		const Integer& d) {
    this->n=p*q;
    this->p=p;
    this->q=q;
    this->d=d;
}

RSADecryptor::~RSADecryptor() {
}

// #compute()
bool RSADecryptor::compute(const Integer& y, Integer& x) const {
    PublicKeyAlgorithmBox box;
    /**
     * Prüfen ob der Geheimtext invalide ist.
     */
    if(y>=n || y < 0)
        return false;
    /**
     * Die Entschlüsselung mit modularer Exponentation durchführen.
     */
    x = box.modularExponentation(y,d,n);
    return true;
}

// #crt()
bool RSADecryptor::crt(const Integer& y, Integer& x) const {
    PublicKeyAlgorithmBox box;
    Integer biggerPrime; // größere Primzahl
    Integer smallerPrime; // kleinere Primzahl
    Integer dummy1;
    Integer dummy2;
    /**
     * Ist der Geheimtext größer als n wird abgebrochen und false zurückgegeben.
     */
    if(y>=n)
        return false;
    /**
    * Zunächst muss festgestellt werden welche der beiden Primzahlen (p,q) die Größere ist.
    */
    if(q==p)
        return false;
    if(q>p){ //feststellen welches die größere Primzahl ist von p und q
        biggerPrime=q;
        smallerPrime=p;
    }
    else{
        biggerPrime=p;
        smallerPrime=q;
    }
    // berechnung von https://www.di-mgt.com.au/crt_rsa.html
    /**
     * Angenommen p ist die größere Primzahl.
     * berechne d mod (p-1) und d mod (q-1)
     * finde mit Erw. Euklid Mult.Inverses (q_inv) von q mod p
     * berechne m1 = y^(d mod p) mod p , m2 = y^(d mod q) mod q
     * Berechne h = (qInv *((m1-m2)+p))%p;
     * Klartext = m2+h*q
     */
    Integer dP = d%(biggerPrime-1);
    Integer dQ = d%(smallerPrime-1);
    Integer qInv;
    box.EEA(smallerPrime,biggerPrime,dummy1,qInv,dummy2);
    Integer m1 = box.modularExponentation(y,dP,biggerPrime);
    Integer m2 = box.modularExponentation(y,dQ,smallerPrime);
    Integer h = (qInv *((m1-m2)+biggerPrime))%biggerPrime;
    x = m2+h*smallerPrime;
    return true;
}

// #garner()
bool RSADecryptor::garner(const Integer& y, Integer& x) const {
    /**
     * Ist der Geheimtext nicht im Geheimtextraum wird false zurückgegeben und es findet keine entschlüsselung statt.
     */
    if(y>=n)
        return false;
    PublicKeyAlgorithmBox box;
    Integer q_Inv; //inverses von q
    /**
     * a,b und das multiplikative Inverse von p mod q werden berechnet.
     * mit der Formel (((a-b)*(q_Inv%p))%p)*q+b; wird der klartext berechnet.
     */
    Integer a = box.modularExponentation(y,d,p);
    Integer b = box.modularExponentation(y,d,q);
    //box.EEA(q,p,dummy1,q_Inv,dummy2);
    box.multInverse(q,p,q_Inv);
    x = (((a-b)*(q_Inv%p))%p)*q+b;
    return true;
}
