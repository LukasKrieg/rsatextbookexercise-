/**
 * \file rsa.cpp
 * \brief Tests rund um das Thema RSA.
 */

#include <iostream>
#include "PublicKeyAlgorithmBox.h"
#include "RSAEncryptor.h"
#include "RSADecryptor.h"
#include "RSAAttack.h"
#include "RSAOracle.h"

using namespace std;

// #rsaParameters()
void rsaParameters() {
    /*********************************************************************
     * Aufgabe 15.
     *********************************************************************/

    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 15) Test generateRSAParams:"<<endl;
    PublicKeyAlgorithmBox box;
    Integer p1,q1,e1,d1;
    box.generateRSAParams(p1,q1,e1,d1);
    cout << "erster test p,q = 256 bit:"<<endl<<"p:"<<p1<<endl<<"q:"<<q1<<endl<<"e:"<<e1<<endl<<"d:"<<d1<<endl<<endl;

    Integer p2,q2,e2,d2;
    box.generateRSAParams(p2,q2,e2,d2,512,50);
    cout << "zweiter test p,q = 512 bit :"<<endl<<"p:"<<p2<<endl<<"q:"<<q2<<endl<<"e:"<<e2<<endl<<"d:"<<d2<<endl;


}

// #rsaDemo()
void rsaDemo() {
    /*********************************************************************
     * Aufgabe 16.
     *********************************************************************/
    // gegebene Werte
    Integer p = Integer("15192846618168946907");
    Integer q = Integer("10041530829891794273");
    Integer e = Integer("141290156426204318982571851806193576543");
    Integer d = Integer("73707354481439936713886319521045114527");
    Integer x = Integer("79372353861768787619084471254314002875");
    Integer y = Integer("47915958473033255778832465116435774510");
    Integer y_1;
    RSAEncryptor rsaEnc(p * q, e);
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 16) Test RSAEncryptor/RSADecryptor:"<<endl;
    cout << "Teilaufgabe f)" << endl;
    cout << "Gegebene Parameter für RSA Test:"<<endl<<"p:\t"<<p<<endl<<"q:\t"<<q<<endl<<"p:\t"<<p<<endl<<"e:\t"<<d<<endl<<"p:\t"<<d<<endl<<"x:\t"<<x<<endl<<"y:\t"<<y;
    // Test RSAEncryptor.compute
    rsaEnc.compute(x, y_1);
    if (y_1 == y)
        cout << endl<<endl<<"RSAEncryptor.compute Test erfolgreich"<<endl<<"y:\t"<<y_1;
    else
        cout << "Fehler in Encryptor.compute"<<endl<<"y_1,y:"<<endl << y_1 << endl << y;
    // Test Decrypt.compute
    RSADecryptor rsaDEC(p, q, d);
    Integer x_1;
    rsaDEC.compute(y, x_1);
    if (x_1 == x)
        cout << endl<<"RSADecryptor.compute Test erfolgreich"<<endl<<"x:\t"<<x_1;
    else
        cout << endl<<"Fehler in RSADecryptor.compute"<<endl<<"x_1,x:"<<endl << x_1 << endl << x;
    // Test crt Decrypt
    x_1 = 0;
    rsaDEC.crt(y, x_1);
    if (x_1 == x)
        cout << endl<<"RSADecryptor.crt Test erfolgreich"<<endl<<"x:\t"<<x_1;
    else
        cout << endl<<"Fehler in RSADecryptor.crt"<<endl<<"x_1,x:"<<endl << x_1 << endl << x;
    // Test garner Decrypt
    x_1 = 0;
    rsaDEC.garner(y, x_1);
    if (x_1 == x)
        cout << endl<<"RSADecryptor.garner Test erfolgreich"<<endl<<"x:\t"<<x_1<<endl;
    else
        cout << endl<<"Fehler in RSADecryptor.garner"<<endl<<"x_1,x:"<<endl << x_1 << endl << x;

}

//#sqrtExercise()
void sqrtExercise() {
	/*********************************************************************
	 * Aufgabe 17.
	 *********************************************************************/
	Integer x1("3157242151326374471752634944"),
			s1,
			x2("11175843681943819792704729"),
			s2,
			x3("3343229819990029117723"),
			s3;

    PublicKeyAlgorithmBox sqrtTest;

    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 17) Test sqrt:"<<endl;
    cout << "Teilaufgabe b)" << endl;
	sqrtTest.sqrt(x1, s1);
    cout << "Vorgabe: x = 3157242151326374471752634944 (Lösung: s = 56189341972712)" << endl;
    cout << "Ergebnis: x = " << x1 << " s = " << s1 << endl << endl;

	sqrtTest.sqrt(x2, s2);
    cout << "Vorgabe: x = 11175843681943819792704729 (Lösung: s = 3343029117723)" << endl;
    cout << "Ergebnis: x = " << x2 << " s = " << s2 << endl << endl;

	sqrtTest.sqrt(x3, s3);
    cout << "Vorgabe: x = 3343229819990029117723 (Lösung: keine Quadratzahl)" << endl;
    cout << "Ergebnis: x = " << x3 << " s = " << s3 << endl;
}

// #factorizingAttack()
void factorizingAttack() {
    /*********************************************************************
     * Aufgabe 18.
     *********************************************************************/

    Integer n = Integer("127869459623070904125109742803085324131");
    Integer phi_n = Integer("127869459623070904102412837477002840200");
    Integer p;
    Integer q;
    RSAAttack rsaAtt;
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 18) Test factorizeN:"<<endl;
    bool succ = rsaAtt.factorizeN(n, phi_n, p, q);

    cout << "Vorgegebene Testvariablen:"<<endl<<"n:\t\t"<<n<<endl<<"Phi(n): "<<phi_n<<endl<<endl<<"Berechnetes Ergebniss:"<<endl;
    if(succ==1){
        cout << "RSAAttack.factorizeN Erfolgreich:" << endl<<"p:" << p << endl << "q:" << q;
        cout << endl<<endl<<"Überprüfung:"<<endl<<"p*q=n:  "<<p*q<<endl<<"Phi(n): "<<(p-1)*(q-1);
    }
    else{
        cout << "FactorizeN fehlgeschlagen:"<<endl<< endl<<"p:" << p << endl << "q:" << q;
        cout << endl<<endl<<"Überprüfung:"<<endl<<"p*q=n:  "<<p*q<<endl<<"Phi(n): "<<(p-1)*(q-1);
    }
}

// #euklidExercise()
void euklidExercise() {
    /*********************************************************************
     * Aufgabe 19.
     *********************************************************************/
    Integer a = 39,
            b = 112,
            d;
    vector<Integer> q;

    PublicKeyAlgorithmBox euklidTest;
    d = euklidTest.euklid(a, b, q);

    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 19) Test euklid mit Kettenbruch:"<<endl;
    cout <<"Teilaufgabe b)" << endl;
    cout <<"Vorgegeben: a = 39, b = 112 (Lösung: d = 1 und q = [0, 2, 1, 6, 1, 4])" << endl;
    cout <<"Ergebnis: d = " << d << ", q = (";
    for(int i = 0; i < q.size()-1; i++) {
        cout << q[i] << ", ";
    }
    cout << q[q.size()-1] << ")" << endl;
}

// #convergentsExercise()
void convergentsExercise() {
    /*********************************************************************
     * Aufgabe 20.
     *********************************************************************/
    Integer a = Integer("39");
    Integer b = Integer("112");
    vector<Integer> c; // sollte c = [0,1,1,7,8,39] sein
    vector<Integer> d; // sollte d = [1,2,3,20,23,112] sein

    PublicKeyAlgorithmBox box;
    box.computeConvergents(a,b,c,d);
    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 20) Test computeConvergence" << endl;
    cout << "Teilaufgabe b)" << endl;
    cout << "Vorgegeben: a = 39, b = 112 (Lösung: c = [0,1,1,7,8,39], d = [1,2,3,20,23,112])" << endl;
    cout << "Ergebnis: ";
    cout <<"c = [";
    for(int i=0;i<c.size();i++){
        cout<<c[i];
    }
    cout << "], ";

    cout <<"d = [";
    for(int i=0;i<d.size();i++){
        cout<<d[i];
    }
    cout << "]" << endl;
}

// #wienerAttack()
void wienerAttack() {
    /*********************************************************************
     * Aufgabe 21.
     *********************************************************************/

    Integer n("224497286580947090363360894377508023561"),
            e("163745652718951887142293581189022709093"),
            p, q;

    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 21) Test wiener attack:"<<endl;
    RSAAttack wiener;
    wiener.wienerAttack(n, e, p, q);
    cout << "gegeben : "<<endl;
    cout << "n = " << n << endl;
    cout << "e = " << e << endl;
    cout << "berechnet:"<<endl;
    cout << "p = " << p << endl;
    cout << "q = " << q << endl;
    cout << "Überprüfung, berechne n = p*q:"<<endl;
    cout << "n = " << p*q << endl;
}

// #oracleExercise()
void oracleExercise() {
    /*********************************************************************
     * Aufgabe 22.
     *********************************************************************/
    Integer p = Integer("16015510136412338011");
    Integer q = Integer("12177032305856164321");
    Integer d = Integer("946975621");
    RSAOracle rsaOrcl(p,q,d);
    Integer y_1 = Integer("116415012259126332853105614449093205668"); // h = 1
    Integer y_2 = Integer("74304303162215663057995326922844871006");  // h = 0
    Integer y_3 = Integer("102949691974634609941445904667722882083"); // h = 0
    Integer y_4 = Integer("42549620926959222864355800078420537413");  // h = 1
    bool b_1 = rsaOrcl.half(y_1);
    bool b_2 = rsaOrcl.half(y_2);
    bool b_3 = rsaOrcl.half(y_3);
    bool b_4 = rsaOrcl.half(y_4);
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 22) Test RSAOracle.half :"<<endl<<y_1<<" : "<<b_1<<endl<<y_2<<"\t : "<<b_2<<
            endl<<y_3<<" : "<<b_3<<endl<<y_4<<"\t : "<<b_4 << endl;
}

// #halfAttack()
void halfAttack() {
    /*********************************************************************
     * Aufgabe 23.
     *********************************************************************/
    Integer p = Integer("12889769717276679053");
    Integer q = Integer("17322528238664264177");
    Integer e = Integer("55051594731967684255289987977028610689");
    Integer d = Integer("149154082258429024247010774747829057473");
    Integer x = Integer("167092961114842952923160287194683529938");
    /*Integer p = Integer("101");
    Integer q = Integer("113");
    Integer e = Integer("3533");
    Integer d = Integer("6597");
    Integer x = Integer("45");*/

    Integer n = p*q;
    Integer y;

    RSAOracle rsaOrcl(p,q,d);

    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 23) Test half Attack :" << endl;
    cout << "Teilaufgabe b)" << endl;
    /**
     * Mit RSAEncryptor x verschlüsseln und anschließend mit halfAttack y knacken.
     */
    RSAEncryptor encryptor(n, e);
    if(!encryptor.compute(x, y)) {
        cout << "Verschlüsseln von x ist schief gelaufen !!!!";
        return;
    }

    RSAAttack attack;
    Integer result = attack.halfAttack(n, e, y, rsaOrcl);

    cout << "Gegegeben:" << endl;
    cout << "p = " << p << endl;
    cout << "q = " << q << endl;
    cout << "e = " << e << endl;
    cout << "d = " << d << endl;
    cout << "x = " << x << endl;
    cout << "Berechnet:" << endl;
    cout << "n = " << n << endl;
    cout << "y = " << y << endl;
    cout << "x (nach half attack) = " << result << endl;
}
// #main()
int main() {
    rsaParameters();
    rsaDemo();
    sqrtExercise();
    factorizingAttack();
    euklidExercise();
    convergentsExercise();
    wienerAttack();
    oracleExercise();
    halfAttack();
    return 0;
}

