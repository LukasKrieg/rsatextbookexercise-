/*
 * RSAOracle.h
 */

#ifndef RSAORACLE_H_
#define RSAORACLE_H_

#include "integer.h"

using namespace std;
using namespace CryptoPP;

class RSAOracle {
private:
    Integer n;
    Integer d;

public:
    RSAOracle(Integer &p, Integer &q, Integer &d);

    virtual ~RSAOracle();

	/**
	 * Die Funktion half entschlüsselt den übergebenen Geheimtext y in den Klartext x und bestimmt ob sich der Klartext in der
	 * Oberen oder Unteren Hälfte von n befindet. Diese Funktion wird in der HalfAttack benutzt um mit einer Binären suche
	 * den Klartext zu finden.
	 * @param y übergebener Geheimtext
	 * @return true wenn Klartext in oberer Hälfte von n, false wenn Klartext in der Unteren Hälfte von n.
	 */
    bool half(const Integer &y) const;

    bool parity(const Integer &y) const;

};

#endif /* RSAORACLE_H_ */
