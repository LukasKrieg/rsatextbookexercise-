/**
 * \file rabin.cpp
 * \brief Tests rund um das Thema Rabin Kryptosystem.
 */

#include <iostream>
#include <vector>
#include "osrng.h"
#include "PublicKeyAlgorithmBox.h"
#include "RabinDecryptor.h"
#include "RabinEncryptor.h"
#include "RabinAttack.h"

using namespace std;

// #rabinParameters()
void rabinParameters() {
	/*********************************************************************
	 * Aufgabe 9.
	 * Generiere zum Testen von randomRabinPrime zwei randomRabinPrime
	 * Primzahlen. Diese wurden jeweils manuell mit Wolfram Alpha überprüft.
	 *********************************************************************/
	PublicKeyAlgorithmBox rabintest;
	Integer p, q;
	unsigned int r = rabintest.randomRabinPrime(p, 256, 50);
	unsigned int r2 = rabintest.randomRabinPrime(q, 256, 50);
	cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 9) Test Random Rabin Prime :"<<endl;
    cout << "p (r=" << r << ", s=50) = " << p << endl;
	cout << "q (r=" << r2 << ", s=50) = " << q << endl;
}

// #sqrtModPrimeExercise()
void sqrtModPrimeExercise() {
	/*********************************************************************
	 * Aufgabe 10.
	 *********************************************************************/
	PublicKeyAlgorithmBox test_sqrtModPrime;
	Integer y1 = Integer("400040001");
 	Integer p1 = Integer("884249923643");
	vector<Integer> v1;
	test_sqrtModPrime.modPrimeSqrt(y1,p1,v1);
	Integer y2 = Integer("644640535034");
	Integer p2 = Integer("868380007367");
	vector<Integer> v2;
	test_sqrtModPrime.modPrimeSqrt(y2,p2,v2);

	cout << endl<<"*********************************************************************"<<endl;
	cout <<"Aufgabe 10) Test sqrtModPrime :"<<endl;
	cout << "y="<<y1<<"\tp="<<p1<<"\t Beide Quadratwurzeln von y:{ v0="<<v1[0]<<"\tv1="<<v1[1]<<" }"<<endl;
	cout << "y="<<y2<<"\tp="<<p2<<"\t Beide Quadratwurzeln von y:{ v0="<<v2[0]<<"\tv1="<<v2[1]<<" }"<<endl;


}

// #rabinDemo()
void rabinDemo() {
	/*********************************************************************
	 * Aufgaben 11 und 12.
	 * Testet die Rabin Encryptor und Decryptor Klasse, indem
	 * die Testwerte aus der Aufgabenstellungen genommen werden.
	 *********************************************************************/
    Integer p("728768879148869666628372866383");
    Integer q("1178365175275537416785439551531");
    Integer x("234131892323212");
    Integer y;

    /**
     * Teste die Verschlüsselung mit n,x
     */
    RabinEncryptor enc(p * q);
    if(!enc.compute(x, y)) {
        cout << "RabinEncryptor compute ist Fehlgeschlagen" << endl;
        return;
    }

    /**
     * Teste die Entschlüsselung mit p,q,y und speichere
     * alle vier Ergebnisse in xv.
     */
    vector<Integer> xv;
    RabinDecryptor dec(p, q);
    if(!dec.compute(y, xv)) {
        cout << "RabinDecryptor compute ist Fehlgeschlagen" << endl;
        return;
    }

    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 11) Test Rabin Encryptor und Decryptor:"<<endl;
    cout << "Teilaufgabe f)" << endl;
    cout << "p = " << p << endl<<"q = " << q << endl<<"x = " << x << endl;
    cout << "Vorgabe Verschlüsselung:" << endl;
    cout << "y = 54817743002848138258673996944" << endl;
    cout << "Berechnung Verschlüsselung:" << endl;
    cout << "y = " << y << endl;
    cout << "Vorgabe Entschlüsselung:" << endl;
    cout << "v0 = 858755868013614750163033981532656431854843747105980613759161" << endl;
    cout << "v1 = 468253878381255984906252283528370901639136709523733710358252" << endl;
    cout << "v2 = 390501989632358765256781698004285530215707037816378795724121" << endl;
    cout << "v3 = 234131892323212" << endl;
    cout << "Berechnung Entschlüsselung:" << endl;
    for(int i = 0; i < 4; i++) {
        cout << "v" << i << " = " << xv[i] << endl;
    }

    /**
     * Teste die Verwendung eines Paddings mithilfe der gleichen Werte wie in Aufgabe 11
     */
    Integer p2("728768879148869666628372866383");
    Integer q2("1178365175275537416785439551531");
    Integer x2("234131892323212");
    Integer y2, decrypted;

    /**
     * Verschlüsselung mit Padding
     */
    RabinEncryptor enc2(p2*q2);
    if(!enc2.compute2(x2, y2)) {
        cout << "RabinEncryptor compute2 ist Fehlgeschlagen" << endl;
        return;
    }

    /**
     * Entschlüsselung mit Padding
     */
    RabinDecryptor dec2(p2, q2);
    if(!dec2.compute2(y2, decrypted)) {
        cout << "RabinDecryptor compute2 ist Fehlgeschlagen" << endl;
        return;
    }

    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 12) Test Rabin Encryptor und Decryptor mit Padding:"<<endl;
    cout << "Verwende die gleichen Test-Werte aus Aufgabe 11." << endl;
    cout << "padding = 987654321" << endl;
    cout << "Berechnung Verschlüsselung:" << endl;
    cout << "y = " << y2 << endl;
    cout << "Berechnung Entschlüsselung:" << endl;
    cout << "x = " << decrypted << endl;
}

// #rabinAttack()
void rabinAttack() {
	/*********************************************************************
	 * Aufgabe 13.
	 *********************************************************************/
	Integer p("728768879148869666628372866383");
	Integer q("1178365175275537416785439551531");

	RabinEncryptor enc(p*q);
	RabinDecryptor dec(p, q);
	RabinAttack rab;
	Integer f;
	int succ = rab.factorize(p*q,f,30,dec);
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 13) Test RabinAttack :"<<endl;
    if(succ==-1)
        cout << "Die RabinAttack ist fehlgeschlagen, es konnte kein Faktor von n berechnet werden."<<endl;
    else {
        cout << "RabinAttack Anzahl Versuche: " << succ << endl << endl<<"Berechnete Primfaktoren:"<< endl<<"f:\t" << f <<endl<<"n/f:"<<(p*q)/f<<endl;
        cout << endl<<"Gegebene Primfaktoren:"<<endl<<"p:\t" << p << endl<<"q:\t" << q << endl;
    }//rab.factorize();
}

// #main()
int main() {

	rabinParameters();
	sqrtModPrimeExercise();
	rabinDemo();
	rabinAttack();

	return 0;
}
