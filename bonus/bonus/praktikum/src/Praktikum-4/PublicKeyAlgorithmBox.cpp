#include <iostream>
#include <iomanip>
#include <sstream>
#include "nbtheory.h"
#include "osrng.h"
#include "PublicKeyAlgorithmBox.h"

using namespace CryptoPP;
using namespace std;

// #EEA()
bool PublicKeyAlgorithmBox::EEA(const Integer &a, const Integer &b,
                                Integer &d, Integer &x, Integer &y) {

    /**
     * Wird b in einem der rekusiven Aufrufe == 0 dann ist a der ggt.
     * der ggt wird über die variable d durch die rekursiven Aufrufe gereicht.
     */
    if (b == 0) {
        d = a;
        x = 1;
        y = 0;
    } else {
        Integer d2;
        Integer x2;
        Integer y2;
        /**
         * für den nächsten rekursiven Aufruf von EEA werden a=b und b=a mod b gesetzt
         */
        EEA(b, a % b, d2, x2, y2);
        d = d2;
        /**
         *  Berechnung der Inversen
         */
        x = y2;
        y = x2 - (a / b) * y2;
    }
    /**
     * sind a und b teilerfremd dann wird true zurückgegeben.
     */
    if (d == 1)
        return true;
    else
        return false;

} // EEA()

// #modularExponentation()
Integer PublicKeyAlgorithmBox::modularExponentation(const Integer &a,
                                                    const Integer &b, const Integer &n) {
    /**
     * d ist der aktuelle Wert mit dem gerechnet wird.
     * Der Algorithmus durchläuft die Bitdarstellung des Exponenten und rechnet bei einer
     * 0: lediglich d = (d * d) %n und bei einer 1: zusätzlich (d * a) % n.
     */
    Integer d = 1;
    for (int i = b.BitCount(); i >= 0; i--) {
        d = (d * d) % n;
        if (b.GetBit(i) == 1) {
            d = (d * a) % n;
        }

    }
    return d;
} // modularExponentation()

// #multInverse()
bool PublicKeyAlgorithmBox::multInverse(const Integer &a, const Integer &n,
                                        Integer &a_inv) {
    Integer d, y;
    /**
     * Berechne mit dem erweiterten euklidischen Algorithmus, den ggt und das potenzielle multiplikative Inverse.
     */
    EEA(a, n, d, a_inv, y);
    /**
     * Wenn ggt(a, n) == 1, dann gibt es ein multiplikatives Inverses
     */

    if(a_inv<0)
        a_inv+=n;
    if (d == 1) {
        return true;
    }
    return false;
} // multInverse()

// #witness()
bool PublicKeyAlgorithmBox::witness(const Integer &a, const Integer &n) {
    Integer d = 1;
    Integer x;
    Integer n_1 = n - 1;
    /**
     * Gleiches Vorgehen wie bei modularerExponentation, n.Bitcount() Schleifendurchläufe quadrieren und multiplizieren die Basis.
     */
    for (int i = n.BitCount(); i >= 0; i--) {
        x = d;
        d = (d * d) % n;
        /**
         * Unterschied zur Modularen Exponentation, es wird in jedem Schleifendurchlauf geprüft ob die Gleichung x*x kongruent 1 mod n gibt.
         * Wenn d == 1 und x!=1 und x!=n-1 ist dann ist die Bedingung erfüllt und a ein Beleg für n.
         */
        if (d == 1 && x != 1 && x != n - 1)
            return true;
        if (n_1.GetBit(i) == 1)
            d = (d * a) % n;
    }
    /**
     * Falls der ggt(a,n)!=1 is dann is a ebenfalls ein Beleg. Primzahlen sind teilerfremd zu allen anderen Zahlen außer sich selbst und 1.
     */
    if (d != 1)
        return true;

    return false;
} // witness()

// randomInteger()
Integer PublicKeyAlgorithmBox::randomInteger(const Integer &n) {
    return Integer(1);
}

// #millerRabinTest()
bool PublicKeyAlgorithmBox::millerRabinTest(Integer &n, unsigned int s) {
    NonblockingRng rng;
    Integer a;
    /**
     * Schleife mit s Durchläufen, in jedem Durchlauf wird eine neue Zufallszahl erzeugt.
     * Mit dieser Zufallszahl und n wird die Funktions Witness aufgerufen. Ist die Zahl n keine Primzahl so
     * besteht in jedem Schleifendurchlauf eine ca 50% Warscheinlichkeit, dass die erzeugte Zufallszahl ein Beleg für n ist.
     */
    for (int i = 0; i < s; i++) {
        a.Randomize(rng, n.BitCount());
        if (witness(a, n))
            return false;
    }
    return true;
} // millerRabinTest()

// #randomPrime()
unsigned int PublicKeyAlgorithmBox::randomPrime(Integer &p,
                                                unsigned int bitlen, unsigned int s) {
    NonblockingRng rng;
    //BlockingRng rng;
    Integer randomnumber;
    unsigned int count = 0; // Die Anzahl der Versuche zählen
    do {
        count++;
        /**
         * Eine zufällige Zahl Würfeln mit der Bitlänge bitlen und anschließend den
         * MillerRabinTest auf dieser Zahl ausführen.
         */
        randomnumber.Randomize(rng, bitlen);
    } while (millerRabinTest(randomnumber, s) == 0);
    p = randomnumber;
    return count;
}// #randomPrime()

unsigned int PublicKeyAlgorithmBox::randomRabinPrime(Integer &p,
                                                     unsigned int bitlen, unsigned int s) {
    NonblockingRng rng;
    do {
        /**
         * Generiere eine Zufallszahl mit bitlen bit length.
         */
        p.Randomize(rng, bitlen);
        /**
         * Führe den Miller Rabin Test aus, mit einer Fehlerwahrscheinlichkeit von 2^(-s)
         * und prüfe ob p ≡ 3 (mod 4) ist.
         */
    } while(!(millerRabinTest(p, s) && (p % 4 == 3)));

    return bitlen;
} // randomRabinPrime()

// #modPrimeSqrt()
bool PublicKeyAlgorithmBox::modPrimeSqrt(const Integer &y, const Integer &p,
                                         vector<Integer> &v) {
    /**
     * Falls p kongruent 3 (mod 4) ist dann werden die Quadratwurzeln von y mod p berechnet und in v gespeichert.
     * Der Exponent wird berechnet mit e=(p+1)/4.
     * y^e (mod p) = x1
     * p - x1 = x2
     */
    Integer e = (p + 1) / 4;
    Integer x_1;
    Integer x_2;
    x_1 = modularExponentation(y, e, p);
    x_2 = p - x_1;
    v.push_back(x_1);
    v.push_back(x_2);
    //issit ===3mod4?
    if (y % p != 3)
        return false;
    else
        return true;
}

Integer PublicKeyAlgorithmBox::euklid(const Integer &a, const Integer &b,
                                      vector<Integer> &q) {
    /**
     * Es wird die iterative Variante des euklidischen Algorithmus durchgeführt.
     */
    Integer r = a;
    Integer r_next = b;
    Integer tmp; // Für Dreieckstausch
    unsigned int m = 1;
    while (r_next != 0) {
        /**
         * Speichere jeweils, wie oft r in r_next reinpasst. Anhand dieser
         * Werte kann der Kettenbruch dargestellt werden.
         */
        q.insert(q.begin() + m - 1, r / r_next);
        tmp = r_next;
        r_next = r - q[m - 1] * r_next;
        r = tmp;
        m++;
    }

    return r;
}

unsigned int PublicKeyAlgorithmBox::computeConvergents(const Integer &a,
                                                       const Integer &b, vector<Integer> &c, vector<Integer> &d) {
    vector<Integer> q;
    Integer c_j;
    Integer d_j;

    /**
     * Berechne den Kettenbruch von a/b und speichere diesen in q.
     */
    euklid(a, b, q);

    /**
     * Ausgabe mit fester Spaltenbreite vorbereiten
     */
    const char sep = ' '; // seperator
    const int smallWidth = 5; // für fünfstellige Spalten
    const int bigWidth = 187; // für maximal 187 stellige BigInteger
    stringstream ss;
    cout << left << setw(smallWidth) << setfill(sep) << "i";
    cout << left << setw(bigWidth) << setfill(sep) << "q[i]";
    cout << left << setw(bigWidth) << setfill(sep) << "i-ter Kettenbruch";
    cout << endl;

    /**
     * Gehe jedes Element vom Kettenbruch durch und speichere jeden
     * Nährungsbruch C_j getrennt in Zähler c_j und Nenner d_j in dem vector c und d.
     */
    //cout <<"i\t\tq[i]\t\ti-ter Kettenbruch:"<<endl;
    for (int j = 0; j < q.size(); j++) {
        if (j == 0) {
            c_j = q[j];
            d_j = 1;
        }
        if (j == 1) {
            c_j = q[j] * c[j - 1] + 1;
            d_j = q[j] * d[j - 1] + 0;
        }
        if (j > 1) {
            c_j = q[j] * c[j - 1] + c[j - 2];
            d_j = q[j] * d[j - 1] + d[j - 2];
        }
        c.push_back(c_j);
        d.push_back(d_j);


        cout << left << setw(smallWidth) << setfill(sep) << j;

        //cout << j << "\t\t";
        ss.str("");
        for(int i = 0; i <= j; i++) {
            ss << q[i];
            ss.seekp(-1,ss.cur);
            ss << ",";
        }
        ss.seekp(-1,ss.cur);
        ss << " ";
        cout << left << setw(bigWidth) << setfill(sep) << ss.str();
        ss.str("");
        ss << c_j << "/" << d_j;
        cout << left << setw(bigWidth) << setfill(sep) << ss.str();
        cout << endl;
    }
    return 1;
}

// #sqrt()
bool PublicKeyAlgorithmBox::sqrt(const Integer &x, Integer &s) const {
    cout << "Berechne sqrt(" << x << ") mit binärer Suche:" << endl;
    /**
     * Die untere und obere Schranke der Binären Suche initalisieren.
     */
    Integer untereSchranke("0"), obereSchranke;
    obereSchranke = x;

    /**
     * guess ist der Mittelwert der aktuellen Suche und
     * guess_squared ist der Mittelwert im Quadrat, um zu schauen, ob guess die Wurzel von x ist.
     */
    Integer guess, guess_squared;

    // round zählt die Runden
    int round = 1;

    /**
     * Ausgabe mit fester Spaltenbreite für die Rundenzahlen und BigInteger.
     */
    const char sep    = ' '; // seperator
    const int roundWidth = 4; // maximal dreistellige Rundenanzahl
    const int bigIntWidth = 80; // maximal 80 stellige BigInteger
    cout << left << setw(roundWidth) << setfill(sep) << "i";
    cout << left << setw(bigIntWidth) << setfill(sep) << "UntereSchranke";
    cout << left << setw(bigIntWidth) << setfill(sep) << "Mittelwert";
    cout << left << setw(bigIntWidth) << setfill(sep) << "ObereSchranke";
    cout << endl;

    /**
     * Solange laufen, bis ein Ergebnis gefunden wurde.
     */
    while (true) {
        /**
         * guess wird auf den Mittelwert der oberen und unteren Schranke gesetzt und
         * über guess_squared kann festgestellt werden, ob der Wert zu groß oder zu klein ist.
         */
        guess = (obereSchranke + untereSchranke) / 2;
        guess_squared = guess * guess;

        /**
         * Alle aktuellen Werte ausgeben. Konvertiert alle BigInteger zunächst zu einem std::string, da eine
         * direkte Ausgabe nicht funktioniert hat.
         */
        stringstream ss;
        cout << left << setw(roundWidth) << setfill(sep) << round;
        ss << untereSchranke;
        cout << left << setw(bigIntWidth) << setfill(sep) << ss.str();
        ss.str("");
        ss << guess;
        cout << left << setw(bigIntWidth) << setfill(sep) << ss.str();
        ss.str("");
        ss << obereSchranke;
        cout << left << setw(bigIntWidth) << setfill(sep) << ss.str();
        cout << endl;

        if (guess_squared == x) {
            /**
             * Wenn der aktuelle Wert im Quadrat x ergibt, dann wurde die Wurzel von x gefunden.
             */
            s = guess;
            return true; // gefunden
        } else if (guess_squared > x) {
            /**
             * Wenn der aktuelle Wert im Quadrat größer als x ist, dann muss der gesuchte Wert
             * kleiner sein. Deshalb wird die obere Schranke angepasst.
             */
            obereSchranke = (((obereSchranke + untereSchranke) + Integer("1")) / Integer("2")); // Aufgerundeter Wert
        } else if (guess_squared < x) {
            /**
             * Wenn der aktuelle Wert im Quadrat kleiner als x ist, dann muss der gesuchte Wert
             * größer sein. Deshalb wird die untere Schranke angepasst.
             */
            untereSchranke = ((obereSchranke + untereSchranke) / Integer("2")); // Abgerundeter Wert
        }

        /**
         * Abbruchbedingung, wenn beide Schranken das Selbe sind oder um eins voneinander abweichen.
         */
        if ((obereSchranke - untereSchranke) <= 1) {
            s = 0;
            return false;
        }

        round++;
    }
}

void PublicKeyAlgorithmBox::generateRSAParams(Integer &p, Integer &q,
                                              Integer &e, Integer &d, unsigned int bitlen, unsigned int s) {
    /**
     * Zufallszahlen p und q werden erzeugt, sie sind warscheinlich primzahlen.
     * Jede der Zahlen hat eine Restwarscheinlichkeit von 1-2^-s eine zusammengesetzte Zahl zu sein.
     */
    randomPrime(p, bitlen, s);
    randomPrime(q, bitlen, s);
    Integer phi_n = (p - 1) * (q - 1);

    /**
     * Zufallszahl e zwischen 1 und phi_n-1 erzeugen, wenn gcd(e, phi_n) == 1 dann kann e als encryptionkey verwendet werden.
     * Erweiterter euklid anwenden um decryptionkey, dass inverse von e zu berechnen.
     */
    Integer gcd, e_inv, phi_n_inv;
    NonblockingRng rng;
    while (gcd != 1) {
        e.Randomize(rng, 1, phi_n - 1);
        EEA(e, phi_n, gcd, e_inv, phi_n_inv);
    }

    d = e_inv % phi_n;
}

// # decimalDivision
string PublicKeyAlgorithmBox::decimalDivison(const Integer &a, const Integer &b, int fixedLength) {
    stringstream ss;
    Integer integerpart, remainder, tmp;
    int precision;
    /**
     * Berechne zunächst den Teil vor dem Komma.
     */
    integerpart = (a/b);
    ss << integerpart;
    ss.seekp(-1,ss.cur); // bewege den Schreibkopf um eins zurück
    ss << ","; // schreibe ',' als letzter Character
    /**
     * Berechne wieviele Kommastellen ausgegeben werden sollen um die fixedLength zu erreichen.
     */
    precision = fixedLength-(int)ss.str().length();

    /**
     * Immer den Rest mal zehn und Modulo b rechnen, um die nächste Nachkommastelle zu berechnen.
     */
    remainder = (a%b)*10; // e.g. 1 * 10 = 10
    while(precision >= 0) {
        tmp = remainder/b; // e.g. 10 / 3 = 3 Rest 1
        ss << tmp;
        ss.seekp(-1,ss.cur);
        remainder = (remainder%b)*10;
        precision--;
    }
    ss << " ";

    return ss.str();
}
