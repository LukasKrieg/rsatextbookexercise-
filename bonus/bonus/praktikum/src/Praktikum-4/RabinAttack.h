/*
 * RabinAttack.h
 */

#ifndef RABINATTACK_H_
#define RABINATTACK_H_

#include "integer.h"
#include "RabinDecryptor.h"

using namespace CryptoPP;

class RabinAttack {
public:
    RabinAttack();

    virtual ~RabinAttack();

    /**
     * Die Sicherheit des Miller Rabin Kryptosystems basiert auf dem Faktorisierungsproblem. Allerdings ist es anfällig gegen
     * Angriffe mit frei wählbarem Geheimtext. Bei der Entschlüsselung gibt es hierbei vier Ergebnisse, eines davon ist der Plaintext,
     * eines der negative Plaintext. Mit den beiden anderen Ergebnissen kann ein Faktor f von n durch f = gcd(Ergebniss-Klartext,n) berechnet werden.
     * Der Faktor f ist ein Teil des Privaten Schlüssels des Rabin Kryptosystems, mit n/f kann auch der andere Teil des
     * geheimen Schlüssels berechnet werden.
     *
     * @param n Öffentlicher Schlüssel des Rabin Kryptosystems, Modul und gleichzeitig Klartext und Geheimtextraum
     * @param f wenn n faktorisiert werden kann dann wird in f der berechnete Faktor von n gespeichert
     * @param max_tries Maximale Anzahl von Versuchen die der Angriff fehlschlagen kann bevor abgebrochen wird.
     * @param rabin_dec Instanz der Klasse RabinDecryptor, ermöglicht Zugriff auf die Entschlüsselungsfunktion.
     * @return war der Angriff erfolgreich wird die Anzahl der Versuche bis zum Erfolg zurückgegeben, war
     * der Angriff nicht erfolgreich dann wird -1 zurückgegeben.
     */
    int factorize(const Integer &n, Integer &f, int max_tries, RabinDecryptor &rabin_dec);
};

#endif /* RABINATTACK_H_ */
