#ifndef _PublicKeyAlgorithmBox_h_
#define _PublicKeyAlgorithmBox_h_

#include <vector>
#include "integer.h"

using namespace std;
using namespace CryptoPP;

class PublicKeyAlgorithmBox {
public:

    /**
     *
     *
     *
     * Gehe jedes Element vom Kettenbruch durch und speichere jeden
     * Nährungsbruch getrennt in Zähler und Nenner in den Vektoren c und d.
     *
     * Je nachdem wie tief der Kettenbruch berechnet wird, nähert man sich dem Ergebnis immer näher an.
     *
     *
     * @param a Erste Zahl auf die der euklid angewendet wird
     * @param b Zweite Zahl auf die der euklid angewendet wird
     * @param c c[i] wird der Zähler des i.ten Nährungsbruchs
     * @param d d[i] wird der Nenner des i.ten Nährungsbruchs
     * @return 1
     */
    unsigned int computeConvergents(const Integer &a, const Integer &b,
                                    vector<Integer> &c, vector<Integer> &d);

    /**
     * Es wird der Kettenbruch von a, b mithilfe des euklidischen Algorithmus berechnet.
     *
     * Beispiel Ablauf des klassischen Algorithmen von Euklid:\n
     * <pre>
     * a = 42, b = 73
     * (42, 73) (73 passt in 42   0 mal) bzw. 42 = 0 · 73 + 42
     * (73, 42) (42 passt in 73   1 mal) bzw. 73 = 1 · 42 + 31
     * (42, 31) (31 passt in 42   1 mal) bzw. 42 = 1 · 31 + 11
     * (31, 11) (11 passt in 31   2 mal) bzw. 31 = 2 · 11 + 9
     * (11, 9)  (9 passt in 11    1 mal) bzw. 11 = 1 · 9 + 2
     * (9, 2)   (2 passt in 9     4 mal) bzw. 9  = 4 · 2 + 1
     * (2, 1)   (1 passt in 2     2 mal) bzw. 2 = 2 · 1 + 0
     * </pre>
     *
     * Die Funktion liefert die partiellen Ergebnisse, wie oft die eine Zahl in die andere Zahl reinpasst.
     * Das Ergebnis des Kettenbruchs wäre: 0, 1, 1, 2, 1, 4, 2\n
     * Somit lässt sich 42/73 als Kettenbruch darstellen als:\n
     * Herkömmlich: 0+(1/(1+(1/(1+(1/(2+(1/(1+(1/(4+(1/2)))))))))))\n
     * Ascii Art:\n
     * <pre>
     *                 1
     *   0 + ---------------------
     *                   1
     *       1 + -----------------
     *                     1
     *           1 + -------------
     *                       1
     *               2 + ---------
     *                         1
     *                   1 + -----
     *                           1
     *                       4 + -
     *                           2
     * </pre>
     *
     * @param a Erste Zahl auf die der euklid angewendet wird
     * @param b Zweite Zahl auf die der euklid angewendet wird
     * @param q Das Ergebnis des Kettenbruchs z.B. 0, 1, 1, 2, 1, 4, 2 wird in q geschrieben
     * @return ggt(a, b)
     */
    Integer euklid(const Integer &a, const Integer &b, vector<Integer> &q);


    /**
     * Berechnet den erweiterten euklidischen Algorithmus in der rekursiven Variante. Zunächst wird der euklidische
     * Algorithmus auf a und b angewendet und die Rekursionstiefe steigt. Wenn bei dem euklidischen Algorithmus b = 0
     * wird ist der tiefste Punkt der Rekursion erreicht. In a steht nun der ggt(a, b). Die Variable d merkt sich den
     * ggt von a,b. Der Rekursionsbaum wird nun abgearbeitet und in jedem Schritt wird x, y berechnet.
     * Wenn der Rekursionsbaum abgearbeitet ist steht das Ergebnis fest:
     * Ist der ggt der beiden Zahlen a und b gleich 1, dann sind die berechneten x und y die Inversen von a und b.
     * Somit gilt für ggt(a,b)=1:
     * a * x % b = 1 -> x ist das multiplikative Inverse von a (mod b)
     * b * y % a = 1 -> y ist das multiplikative Inverse von b (mod a)
     *
     * Beispiel für die Zahlen a = 102 und b = 75:
     * <pre>
     * |-----|----|-------|---|-----|-----|
     * |  a  | b  | ⌊a/b⌋ | d |  x  |  y  |
     * |-----|----|-------|---|-----|-----|
     * | 102 | 75 | 1     | 3 | -11 |  15 |
     * |-----|----|-------|---|-----|-----|
     * |  75 | 27 | 2     | 3 |   4 | -11 |
     * |-----|----|-------|---|-----|-----|
     * |  27 | 21 | 1     | 3 |  -3 |   4 |
     * |-----|----|-------|---|-----|-----|
     * |  21 |  6 | 3     | 3 |   1 |  -3 |
     * |-----|----|-------|---|-----|-----|
     * |   6 |  3 | 2     | 3 |   0 |   1 |
     * |-----|----|-------|---|-----|-----|
     * |   3 |  0 | -     | 3 |   1 |   0 |
     * |-----|----|-------|---|-----|-----|
     * </pre>
     * @param a ist eine der beiden übergebenen Zahlen für die der ggt und das Inverse ermittelt werden soll.
     * @param b ist die andere der beiden Zahlen für die der ggt und das Inverse ermittelt werden soll.
     * @param d in die Variable d wird der ggt der beiden Parameter a und b gespeichert.
     * @param x ist das Inverse von a, wenn der ggt(a,b)=1.
     * @param y ist das Inverse von a, wenn der ggt(a,b)=1.
     * @return Der Returnwert sagt aus ob die beiden Zahlen a und b teilerfremd sind oder nicht. true = Teilerfremd, false= nicht teilerfremd.
     */
    bool EEA(const Integer &a, const Integer &b, Integer &d,
             Integer &x, Integer &y);

	/**
	 * Bei der modularen Exponentation wird a^b mod n berechnet, ohne das die Zahlen zu groß werden.
	 * Um den Algorithmus in logarithmischer Laufzeit zu berechnen, wird der Exponent anhand der Bitschreibweise
	 * des Exponenten indirekt in zweier Potenzen geteilt.
	 * Beispiel von 2^25 mod 100:\n
	 * 2^25 = ((((1^2 * 5)^2 * 5)^2)^2)^2 * 5 = 2^16 * 2^8 * 2^1\n
	 * 2^25 mod 100 = ((((((((((1^2 mod 100) * 5) mod 100)^2 mod 100) * 5) mod 100)^2 mod 100)^2 mod 100)^2 mod 100) * 5) mod 100
	 *
	 * @param a Die Basis
	 * @param b Der Exponent
	 * @param n Das Modulo
	 * @return Das Ergebnis von a^b mod n
	 */
    Integer modularExponentation(const Integer &a, const Integer &b,
                                 const Integer &n);

    /**
     * Berechnet das multiplikative Inverse von a (mod n). Dabei wird der Erweiterte Euklid auf a und n angewendet.
     * Siehe erweiterter Euklid für mehr Informationen.
     *
     * @param a Big Integer für den das multiplikatives Inverse berechnet werden soll
     * @param n Big Integer Modulo
     * @param a_inv Das multiplikatives Inverse von a
     * @return true => ggt(a, n) = 1 bzw. es existiert ein multiplikatives Inverses für a mod n.
     */
    bool multInverse(const Integer &a, const Integer &n, Integer &a_inv);

    /**
     * Die Funktion witness testet ob a ein Beleg dafür ist, dass n keine Primzahl ist.
     * Sollte n keine Primzahl sein dann existieren hierfüg mindestens (n-1)/2 Belege in {1...n-1}
     * Die Funktion führt eine Modulare Exponentation a^n-1 mod n durch und testet bei jedem Durchlauf ob es eine
     * Lösung für die Gleichung x^2 Kongruent 1 mod n gibt. Gibt es eine Lösung für x außer 1 oder n-1 dann ist n keine Primzahl.
     * Mit der Funktion Witness kann somit mit einer ca. 50% Warscheinlichkeit bewiesen werden, dass n keine Primzahl ist.
     * Diese Eigenschaft wird in der Funktion MillerRabinTest genutzt.
     * @param a Die übergebene Integer Zahl die ein beleg für n sein könnte.
     * @param n Die Zahl für welche ein Beleg gefunden werden soll, dass sie keine Primzahl ist.
     * @return ist a ein Beleg dafür, dass n eine zusammengesetzte Zahl ist, wird true zurückgegeben, andernfalls false.
     */
    bool witness(const Integer &a, const Integer &n);

    /** Der MillerRabinTest testet ob eine Zahl zusammengesetzt ist, indem in s Schleifendurchläufen je eine Zufällige Zahl a erzeugt wird.
     * Mit der Funktion Witness wird getestet ob a ein Beleg für n ist. Sprich a belegt, dass n zusammengesetzt ist.
     * Da es für eine zusammengesetzte Zahl in {1...n-1} ca (n-1)/2 Belege gibt, besteht bei jedem Schleifendurchlauf
     * eine ca 50% Chance einen Beleg zu finden.
     * Bei s Schleifendurchläufen ist die Chance, für eine zusammengesetzte Zahl einen Beleg zu finden 1-(2^-s).
     * Da der MillerRabinTest nur mit Warscheinlichkeit von 1-(2^-s) einen Beleg finden kann, besitzt er einen einseitigen Fehler.
     * Wird kein Beleg gefunden ist die Zahl nur mit einer Warsch. von 1-(2^⁻s) eine Primzahl.
     * @param n Die zu testende Zahl.
     * @param s Das Qualitätskriterium, dass die Anzahl der Schleifendurchläufe und die Chance auf einen Beleg bestimmt.
     * @return Wurde ein Beleg gefunden und n ist keine Primzahl dann wird false zurückgegeben, andernfalls true.
     */
    bool millerRabinTest(Integer &n, unsigned int s);

    Integer randomInteger(const Integer &n);

    /**
     * Diese Funktion gibt in der Variablen p eine bitlen Bit Zufallszahl zurück. Dabei ist die Wahrscheinlichkeit
     * das p eine Primzahl ist: 1 - 2^(-s).
     *
     * @param p Die Zufällige Primzahl, welche zurückgegeben wird
     * @param bitlen Die Größe der zu generierenden Zufallszahlen in Bit
     * @param s Die Fehlerwahrscheinlichkeit, dass p eine Primzahl ist: 1 - 2^(-s).
     * @return Die Anzahl der Versuche, eine zufällige Primzahl zu finden
     */
    unsigned int randomPrime(Integer &p, unsigned int bitlen, unsigned int s);

	/**
	 * Generiert eine zufällige Rabin Primzahl. Eine Rabin Primzahl ist eine normale Primzahl, bei der zusätzlich
	 * p ≡ 3 (mod 4) gilt.
	 *
	 * @param p Die generierte Primzahl wird in diese Variable geschrieben.
	 * @param bitlen Die Bit Länge der zu generierenden Primzahl.
	 * @param s Die Fehlerwahrscheinlichkeit von  2^(-s).
	 * @return die Bitlänge der generierten Primzahl.
	 */
    unsigned int randomRabinPrime(Integer &p, unsigned int bitlen,
                                  unsigned int s);

    /**
     * Die Funktion modPrimeSqrt berechnet die Quadratwurzeln der Zahl y (mod p).
     * Die Quadratuwrzeln werden folgendermaßen berechnet:
     * x_1= y^((p+1)/4) (mod p).
     * x_2 = p - x_1
     *
     * @param y Zahl für die beide Quadratwurzeln mod p berechnet werden sollen.
     * @param p Modulo
     * @param v Vector der die Quadratwurzeln speichert.
     * @return Wenn p kongruent 3 (mod 4) und die Quadratuwrzeln berechnet wurden = true, sonst false.
     */
    bool modPrimeSqrt(const Integer &y, const Integer &p, vector<Integer> &v);

    /**
     * Berechnet mit der Binären Suche die Quadratwurzel von x und speichert das Ergebnis in s.
     *
     * @param x Für diese Zahl wird versucht die Quadratwurzel zu finden.
     * @param s Das Ergebnis wird in s gespeichert. 0 wenn kein Ergebnis gefunden werden konnte.
     * @return true, wenn gefunden, sonst false
     */
    bool sqrt(const Integer &x, Integer &s) const;

    /**
     * Generiert die für RSA benötigten Parameter.
     * Zunächst werden 2 Primzahlen p und q erzeugt und phi(n) = (p-1)*(q-1) berechnet.
     * phi(n) ist die Anzahl der Teilerfremden Zahlen zu n.
     * Der Encryptionkey e wird sollange zufällig erzeugt bis die Bedingung: gcd(phi(n),e)=1 erfüllt ist.
     * Wurde ein e gefunden dann wird mittels erweitertem Euklid von e und Phi(n) der Decryptionkey d berechnet.
     * Die berechneten Werte werden in die Übergabeparameter gespeichert.
     * @param p speichert Zufallszahl mit der Bitlänge "bitlen" die mit einer Warscheinlichkeit von 1- 2^-s eine Primzahl ist.
     * @param q speichert Zufallszahl mit der Bitlänge "bitlen" die mit einer Warscheinlichkeit von 1- 2^-s eine Primzahl ist.
     * @param e speichert Encryptionkey für RSA.
     * @param d speichert Decryptionkey für RSA.
     * @param bitlen Bitlänge der generierten Zufallszahlen p und q. default = 256.
     * @param s Beinflusst die Warscheinlichkeit mit der
     */
    void generateRSAParams(Integer &p, Integer &q, Integer &e, Integer &d,
                           unsigned int bitlen = 256, unsigned int s = 30);

    /**
     * Diese Funktion teilt zwei BigInteger und gibt das Ergebnis mit Nachkommastellen aus.
     * Das Ergebnis der Ganzzahldivision wird immer ausgegeben. Wieviele Nachkommastellen die Funktion
     * zusätzlich ausgibt, bestimmt der Parameter fixedLength. So füllt die Funktion den Resultats-String
     * mit Nachkommastellen auf bis die fixedLength des Strings erreicht ist.
     *
     * @param a Dividend/Zähler
     * @param b Divisor/Nenner
     * @param fixedLength Die Länge des Strings, welcher zurückgegeben wird.
     * @return Das Ergebnis der Division a/b mit der Länge von fixedLength
     */
    string decimalDivison(const Integer &a, const Integer &b, int fixedLength);
};

#endif
