/*
 * RSAEncryptor.h
 */

#ifndef RSAENCRYPTOR_H_
#define RSAENCRYPTOR_H_

#include "integer.h"

using namespace CryptoPP;

class RSAEncryptor {
private:
	/** Modulo n zur Verschlüsselung */
	Integer n;
    /** Exponent e zur Verschlüsselung */
	Integer e;

public:
	/**
	 * Konstruktor von RSAEncryptor. Die Parameter werden zu den entsprechenden privaten Variablen im Objekt zugewiesen.
	 *
	 * @param n Der Modulo für die Verschlüsselung
	 * @param e Der Exponent für die Verschlüsselung
	 */
	RSAEncryptor(const Integer& n, const Integer& e);
	virtual ~RSAEncryptor();

    /**
     * Berechnet den Geheimtext mit der Formel:\n
     * y = x^e mod n
     *
     * @param x Der Klartext, welcher verschlüsselt werden soll
     * @param y Der Geheimtext, der berechnet wird
     * @return false, wenn (x>=n || x < 0), sonst true
     */
	bool compute(const Integer& x, Integer& y) const;
};

#endif /* RSAENCRYPTOR_H_ */
