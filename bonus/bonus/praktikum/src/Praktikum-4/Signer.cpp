// #modularExponentation()

//using namespace CryptoPP;
using namespace std;

#include "integer.h"
#include <string.h>
#include <iostream>

CryptoPP::Integer n2 = CryptoPP::Integer("4294967311*4294967357");
CryptoPP::Integer n = CryptoPP::Integer("18446744400127067027");
CryptoPP::Integer phi = CryptoPP::Integer("18446744391537132360");
CryptoPP::Integer e = CryptoPP::Integer("65537");
CryptoPP::Integer d = CryptoPP::Integer("161282703455311913");

CryptoPP::Integer modExp(const CryptoPP::Integer &a, const CryptoPP::Integer &b, const CryptoPP::Integer &n) {
    CryptoPP::Integer d = 1;
    for (int i = b.BitCount(); i >= 0; i--) {
        d = (d * d) % n;
        if (b.GetBit(i) == 1) {
            d = (d * a) % n;
        }

    }
    return d;
} // modularExponentation()

bool decrypt(const CryptoPP::Integer &y, CryptoPP::Integer &x){

    /**
     * Prüfen ob der Geheimtext invalide ist.
     */
    if (y >= n || y < 0)
        return false;
    /**
     * Die Entschlüsselung mit modularer Exponentation durchführen.
     */
    x = modExp(y, d, n);
    return true;
}
bool check(string y){
    string str ("Ich");
    string str1 ("Bob");
    string str2 ("schulde");
    string str3 ("dir");
    string str4 ("500");
    string str5 ("€");
    string str6 ("Ciphertext");
    if (y.find(str) != string::npos||
            y.find(str1) != string::npos||
            y.find(str2) != string::npos||
            y.find(str3) != string::npos||
            y.find(str4) != string::npos||
            y.find(str5) != string::npos||
            y.find(str6) != string::npos
            ) {
        return true;
    }
    return false;

}

int main(int argc,char *argv[]){
    CryptoPP::Integer x = CryptoPP::Integer("0");
    printf(argv[0],argv[1]);
    if(check(argv[1])){
        cout << "Ich bin doch nicht blöd, sowas unterschriebe ich nicht!!!";
        return false;
    }
    else{
        decrypt(CryptoPP::Integer(argv[1]), x);
        cout <<"Die Nachricht sieht gut aus, meine Signatur dazu ist:"<<x;
    }

    return 0;
}

