/*
 * RabinDecryptor.cpp
 */

#include <vector>
#include <sstream>
#include <iostream>
#include "RabinDecryptor.h"
#include "PublicKeyAlgorithmBox.h"

RabinDecryptor::RabinDecryptor(const Integer& p, const Integer& q,
		const Integer& padding) {
    RabinDecryptor::p = p; // Rabin Primzahl p
    RabinDecryptor::q = q; // Rabin Primzahl q
    RabinDecryptor::padding = padding; // Es kann mit compute2 zusätzlich ein Offset im Klartext detektiert werden
    /**
     * Die Padding Länge wird in offset geschrieben.
     * Dazu wird der Integer in einen String konvertiert und die Länge ausgelesen.
     */
    std::stringstream ss;
    ss << padding;
    RabinDecryptor::offset = ss.str().length();
}

// #compute()
bool RabinDecryptor::compute(const Integer& y, vector<Integer>& xv) {
    /**
     * Berechne n aus p * q
     */
    Integer n = p * q;

    /**
     * Berechne für p und q jeweils den ggt und das multiplikative Inverse mq, mp
     * mithilfe des EEA.
     */
    PublicKeyAlgorithmBox box;
    Integer d, mp, mq;
    box.EEA(p, q, d, mq, mp);
    /**
     * Prüfe ob ggt(p, q) = 1
     */
    if(d != 1) {
        return false;
    }

    vector<Integer> xi, xj;
    /**
     * Anwenden einer Formel, welche zwei mögliche Lösungen für die Quadratwurzel ausgibt pro Rechnung.
     * xi[0] und xi[1] sind die Lösungen der Gleichung: y^2 ≡ a (mod p)
     * xj[0] und xj[1] sind die Lösungen der Gleichung: y^2 ≡ a (mod q)
     */
    box.modPrimeSqrt(y, p, xi);
    box.modPrimeSqrt(y, q, xj);

    /**
     * Anwendung des chinesischen Restsatzes, um vier mögliche Ergebnisse für die Entschlüsselung in xv
     * zu schreiben.
     */
    for(int i = 0; i < 2; i++) {
        for(int j = 0; j < 2; j++) {
            xv.push_back((xi[i] * q * mp + xj[j] * p * mq) % n);
        }
    }

    return true;
}

bool RabinDecryptor::compute(const Integer& y, Integer& x) {
    vector<Integer> xv;

    /**
     * Hole alle vier möglichen Klartexte
     */
    if(!compute(y, xv)) {
        return false;
    }

    /**
     * Speichere den ersten Klartext in x
     */
    x = xv[0];

    return true;
}

// #compute2()
bool RabinDecryptor::compute2(const Integer& y, Integer& x) {

    vector<Integer> oldresult;
    compute(y, oldresult);

    /**
     * Konvertiere alle Ergebnisse und das padding zu einem String, um
     * die Werte später zu vergleichen.
     */
    std::stringstream ss;
    ss << padding;
    string paddingstr = ss.str();

    vector<string> oldresultstr;
    for(int i = 0; i < 4; i++) {
        std::stringstream ss;
        ss << oldresult[i];
        oldresultstr.push_back(ss.str());
    }

    /**
     * Vergleiche die Substrings und finde den Plaintext mit dem Padding.
     * Dabei wird jeweils die Länge des Paddings als Substring aus dem hinteren Teil des Results extrahiert
     * und mit dem Padding verglichen. Wenn diese übereinstimmen wird das Ergebnis in x gespeichert.
     */
    long start;
    string sub;
    for(int i = 0; i < 4; i++) {
        start = Integer(oldresultstr[i].length() - offset).ConvertToLong();
        if(start < 0) continue; // Padding ist länger als der Resultsinteger
        sub = oldresultstr[i].substr((unsigned int)start, (unsigned int)offset.ConvertToLong());
        if(sub == paddingstr) {
            x = oldresult[i];
            return true;
        }
    }
    return false;
}

RabinDecryptor::~RabinDecryptor() {
}

