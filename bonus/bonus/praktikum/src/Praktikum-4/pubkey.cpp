/**
 * \file pubkey.cpp
 * \brief Testet Hilfsfunktionen für Public Key Kryptosysteme
 */

#include <iostream>
#include <set>
#include "osrng.h"
#include "PublicKeyAlgorithmBox.h"
// mod_exp test
#include "nbtheory.h"

using namespace std;

void integerExercise() {
    /*********************************************************************
     * Aufgabe 2.
     *********************************************************************/
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 2) Integer Excercise"<<endl;
    // Aufgabe 2a)
    Integer a = Integer(23975);
    Integer b = Integer("12345678900987654321");
    Integer c = Integer::Power2(10);
    // Aufgabe 2b)
    Integer result = (a + c) * b % 50001;
    cout << "Berechnung ( 23975 + 2^10 ) * 12345678900987654321 mod 50001 = ";
    cout << result << endl;
}

void eeaExercise() {
    /*********************************************************************
     * Aufgabe 3.
     * a)a = 2987 und b = 1279865 (Lösung:d=1,x= -128972, y=301)
     * b)a = 78845945, b = 2503417846 (Lösung: d=33767, x = 10573, y= -333)
     * c)a = 57913313, b = 173739939 (Lösung: d=57913313, x=1, y=0)
     *********************************************************************/
    Integer a, b, d, x, y, a2, b2, d2, x2, y2, a3, b3, d3, x3, y3;
    a = 2987;
    b = 1279865;
    a2 = 78845945;
    b2 = 2503417846;
    a3 = 57913313;
    b3 = 173739939;
    PublicKeyAlgorithmBox testEEA;
    bool r1 = testEEA.EEA(a, b, d, x, y);
    bool r2 = testEEA.EEA(a2, b2, d2, x2, y2);
    bool r3 = testEEA.EEA(a3, b3, d3, x3, y3);
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 3) Test EEA(Eweiterter Euklidischer Algorithmus):"<<endl<<""<<endl;
    cout << "Teilaufgabe a):"<<endl;
    cout << "Vorgabe:\ta=2987 b=1279865 (Lösung: d=1 x=-128972 y=301)"<<endl;
    cout << "Berechnung:\ta=" << a << " b=" << b << " d=" << d << " x=" << x << " y=" << y << " "<<endl<<"a und b Teilerfremd:"<<r1<< endl<<endl;
    cout << "Teilaufgabe b):"<<endl;
    cout << "Vorgabe:\ta=78845945 b=2503417846 (Lösung: d=33767 x=10573 y=-333)"<<endl;
    cout << "Berechnung:\ta=" << a2 << " b=" << b2 << " d=" << d2 << " x=" << x2 << " y=" << y2 << endl<<"a und b Teilerfremd:"<<r2<< endl<<endl;
    cout << "Teilaufgabe c):"<<endl;
    cout << "Vorgabe:\ta=57913313 b=173739939 (Lösung: d=57913313 x=1 y=0)"<<endl;
    cout << "Berechnung:\ta=" << a3 << " b=" << b3 << " d=" << d3 << " x=" << x3 << " y=" << y3 << endl<<"a und b Teilerfremd:"<<r3<< endl;
}

void invExercise() {
    /*********************************************************************
     * Aufgabe 4.
     * r -> returnvalue
     * a) a=10353 n=820343 (Lösung: a_inv=21711, r=true)
     * b) a=10353 n=820344 (Lösung: r=false)
     * c) a=562312 n=57913313 (Lösung: a_inv=53494466, r=true)
     *********************************************************************/
    Integer a = 10353,
            n = 820343,
            a_inv,
            a2= 10353,
            n2= 820344,
            a2_inv,
            a3= 562312,
            n3= 57913313,
            a3_inv;
    PublicKeyAlgorithmBox testMultInv;
    bool r = testMultInv.multInverse(a, n, a_inv);
    bool r2 = testMultInv.multInverse(a2, n2, a2_inv);
    bool r3 = testMultInv.multInverse(a3, n3, a3_inv);
    cout << endl<<"*********************************************************************"<<endl;
    cout << "Aufgabe 4) Test Multiplikatives Inverses:"<<endl<<endl;
    cout << "Teilaufgabe a):"<<endl;
    cout << "Vorgabe:\ta=10353 n=820343 (Lösung: a_inv=21711, r=true)"<<endl;
    cout << "Berechnung:\ta=" << a << " n=" << n << " a_inv=" << a_inv << " r=" << r << endl<<endl;
    cout << "Teilaufgabe b):"<<endl;
    cout << "Vorgabe:\ta=10353 n=820344 (Lösung: r=false)"<<endl;
    cout << "Berechnung:\ta=" << a2 << " n=" << n2 << " r=" << r2 << endl<<endl;
    cout << "Teilaufgabe c):"<<endl;
    cout << "Vorgabe:\ta=562312 n=57913313 (Lösung: a_inv=53494466, r=true)"<<endl;
    cout << "Berechnung:\ta=" << a3 << " n=" << n3 << " a_inv=" << a3_inv << " r=" << r3 << endl;
}

void modexpExercise() {
    /*********************************************************************
     * Aufgabe 5.
     * Die Modulare Exponentation wird getestet aus PublicKeyAlgorithmBox.
     * a) 2^100000 mod 23 (Lösung: 12)
     * b) 2343947997^765 mod 111 (Lösung: 105)
     *********************************************************************/
    PublicKeyAlgorithmBox modExpTest;
    Integer ergA = modExpTest.modularExponentation(2,100000,23);
    Integer ergB = modExpTest.modularExponentation(2343947997,765,111);
    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 5) Test Modulare Exponentation:"<<endl;
    cout <<"a) Modulare Exponentation 2 ^ 100000 mod 23 = "<<ergA<<"\t(Lösung aus Aufgabenstellung: 12)"<<endl;
    cout <<"b) Modulare Exponentation 2343947997 ^ 765 mod 111 = "<<ergB<<"\t(Lösung aus Aufgabenstellung: 105)"<<endl;

}

void randExercise() {
    /*********************************************************************
     * Aufgabe 6.
     * - Zufallszahl mit 128 bit und 1024 bit erzeugen.
     * - Blocking und NonBlocking testen
     *********************************************************************/
    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 6) Randomize Test :"<<endl<<endl;
    /**
     * Test Blocking RNG"<<endl<<"
     * Encapsulates /dev/random on Linux, OS X and Unix; and /dev/srandom on the BSDs.
     */
    cout << "- BlockingRng"<<endl;
    BlockingRng rng;
    Integer r128, r1024;
    r128.Randomize(rng, 128);
    cout <<endl<< "128 bit:" << endl << r128 <<  endl;
    r1024.Randomize(rng, 1024);
    cout <<endl<< "1024 bit:" << endl << r1024 << endl<< endl;

    /**
     * Encapsulates CryptoAPI's CryptGenRandom() or CryptoNG's BCryptGenRandom() on Windows, or /dev/urandom on Unix and compatibles.
     */
    cout << "- NonBlockingRng"<<endl;
    NonblockingRng rng_nonblocking;
    Integer rn128, rn1024;
    rn128.Randomize(rng_nonblocking, 128);
    cout <<endl<< "128 bit:" << endl << rn128 << endl;
    rn1024.Randomize(rng_nonblocking, 1024);
    cout <<endl<< "1024 bit:" << endl << rn1024 << endl;
}

void millerRabinExercise() {
    /*********************************************************************
     * Aufgabe 7.
     *********************************************************************/
    PublicKeyAlgorithmBox mrtest;
    Integer a = Integer("279226292160650115722581212551219487007");
    Integer b = Integer("247278711133334795867191516244139839983");
    Integer c = Integer("192172622525902080249109244057747132167");
    Integer d = Integer("177387942943728133030691912202779547031");
    bool isPrimeA=mrtest.millerRabinTest(a,100);
    bool isPrimeB=mrtest.millerRabinTest(b,100);
    bool isPrimeC=mrtest.millerRabinTest(c,100);
    bool isPrimeD=mrtest.millerRabinTest(d,100);
    cout << endl<<"*********************************************************************"<<endl;
    cout <<"Aufgabe 7) MillerRabinTest :"<<endl<<endl;
    cout << a << " ist eine Primzahl: "<<isPrimeA<< endl;
    cout << b << " ist eine Primzahl: "<<isPrimeB<< endl;
    cout << c << " ist eine Primzahl: "<<isPrimeC<< endl;
    cout << d << " ist eine Primzahl: "<<isPrimeD<< endl;



}

int main(int argc, char **argv) {

    integerExercise();
    eeaExercise();
    invExercise();
    modexpExercise();
    randExercise();
    millerRabinExercise();

    return 0;

}
