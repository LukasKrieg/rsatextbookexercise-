/*
 * RSAOracle.cpp
 */

#include <cassert>
#include <iostream>
#include "RSAOracle.h"
#include "PublicKeyAlgorithmBox.h"

using namespace std;

RSAOracle::RSAOracle(Integer &p, Integer &q, Integer &d) {
    this->n = p * q;
    this->d=d;
}

RSAOracle::~RSAOracle() {
}

// #half()
bool RSAOracle::half(const Integer &y) const {
    PublicKeyAlgorithmBox box;
    /**
     * Entschlüssele den Klartext x mit y^d mod n
     */
    Integer x = box.modularExponentation(y,d,n);
    /**
     * Liegt der Klartext in der oberen Hälfte von n dann gebe True zurück, sonst false
     */
    return !(x < n / 2);
}

// #parity()
bool RSAOracle::parity(const Integer &y) const {
    return false;
}
