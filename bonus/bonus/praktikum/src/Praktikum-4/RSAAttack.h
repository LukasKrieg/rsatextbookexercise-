/*
 * RSAAttack.h
 */

#ifndef RSAATTACK_H_
#define RSAATTACK_H_

#include "integer.h"
#include "RSAOracle.h"

using namespace CryptoPP;

class RSAAttack {
public:
	RSAAttack();
	virtual ~RSAAttack();

	/**
	 * Ist die eigentlich geheime Variable Phi(n)  des RSA Kryptosystems bekannt kann n Faktorisiert werden.
	 * Dadurch können die beiden geheimen Primzahlen p und q ermittelt werden.
	 * Durch die Kenntniss von Phi(n) kann über die Mitternachtsformel p und q berechnet werden.
	 * Dabei sind:
	 *      a = 1
	 *      b = Phi(n)-n-1
	 *      c = n
	 * Ist das Ergebniss Richtig, p*q=n dann wird true zurückgegeben und p und q sind in den beiden Übergabeparametern
	 * p und q gespeichert.
	 * @param n Teil des Öffentlichen Schlüssels, p*q.
	 * @param phi_n Nötig zur Faktorisierung von n in nicht polynomialer Laufzeit.
	 * @param p hier wird p einer der geheimen Primfaktoren von n gespeichert nach der Faktorisierung.
	 * @param q hier wird q einer der geheimen Primfaktoren von n gespeichert nach der Faktorisierung.
	 * @return war die Faktorisierung erfolgreich wird True zurückgegeben, ansonsten false.
	 */
	bool factorizeN(const Integer& n, const Integer& phi_n, Integer& p, Integer& q) const;

    /**
     * Mit dem Wiener Angriff Kann bei ungünstig gewählten Primzahlen p und q das n faktorisiert werden.
     * Zunächst werden die Konvergenten des Bruchs e/n berechnet.
     * Mit allen Konvergenten wird versucht ein Mögliches Phi(n) zu berechnen.
     * Wenn das Mögliche Phi(n) eine ganze Zahl ist, dann wird versucht anhand von Phi(n), n zu faktorisieren.
     * war die Faktorisierung erfolgreich dann wird true zurückgegeben.
     * @param n das n, dass es zu faktorisieren gilt.
     * @param e der encryption exponent e der teil des öffentlichen Schlüssels ist.
     * @param p Übergabeparameter in welches der erste Faktor von n gepeichert werden soll.
     * @param q Übergabeparameter in welches der zweite Faktor von n gepeichert werden soll.
     * @return true wenn Faktorisierung erfolgreich, sonst false.
     */
	bool wienerAttack(const Integer& n, const Integer& e, Integer& p, Integer& q) const;

	/**
	 * Die half Attack führt eine Binäre Suche nach dem Klartext durch. Dafür wird über die Funktion Half
	 * und Shifts jeweils das höchstwertigste Bit vom Klartext ausgelesen, welche anschließend für die binäre Suche
	 * nach dem Klartext verwendet werden.
	 *
	 * @param n Der Modulo des öffentlichen Schlüssels
	 * @param e Der Exponent des öffentlichen Schlüssels
	 * @param y Der verschlüsselte Text, welcher mit n und e verschlüsselt wurde.
	 * @param rsa_oracle Ein Orakel, welches die Funktion Half beinhaltet und jeweils das höchstwertigste Bit nennen kann
	 * @return Der entschlüsselte Text als BigInteger
	 */
	Integer halfAttack(const Integer& n, const Integer& e, const Integer& y, RSAOracle& rsa_oracle) const;

};

#endif /* RSAATTACK_H_ */
