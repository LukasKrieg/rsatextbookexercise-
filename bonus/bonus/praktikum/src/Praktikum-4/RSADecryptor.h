/*
 * RSADecryptor.h
 */

#ifndef RSADECRYPTOR_H_
#define RSADECRYPTOR_H_

#include "integer.h"

using namespace CryptoPP;

class RSADecryptor {
private:
    /** Teil des privaten Schlüssels */
	Integer p;
    /** Teil des privaten Schlüssels */
	Integer q;
    /** Teil des privaten Schlüssel bzw. Inverses von e */
	Integer d;
    /** Der Modulo für die Entschlüsselung */
	Integer n;

public:
	/**
	 * Konstruktor von RSAEncryptor. Die Parameter werden zu den entsprechenden privaten Variablen im Objekt zugewiesen.
	 *
	 * @param p Der private Teil des RSA Schlüssels p
	 * @param q Der private Teil des RSA Schlüssels q
	 * @param d Der private Teil des RSA Schlüssels d (multiplikatives Inverses von e)
	 */
	RSADecryptor(const Integer& p, const Integer& q, const Integer& d);
	virtual ~RSADecryptor();

    /**
     * Entschlüsselung des Geheimtextes mithilfe folgender Formel:
     * x = y^d mod n
     * @param y Der Geheimtext, welcher entschlüsselt werden soll
     * @param x Der Plaintext, der bei der Entschlüsselung herauskommt
     * @return false, wenn (y>=n || y < 0), sonst true
     */
	bool compute(const Integer& y, Integer& x) const;
	/**
	 * Der Chinesische Restsatz kann genutzt werden um die Entschlüsselung im RSA Kryptosystem zu beschleunigen, da hier
	 * mit kleineren Zahlen gerechnet wird als bei der Konventionellen Methode.
	 * Die größere der beiden Primzahlen wird hier als p bezeichnet.
	 * Es wird zunächst dP = d mod (p-1) und dQ = d mod (q-1) berechnet.
	 * Mit dem Erweiterten Euklidischen Algorithmus wird das Multiplikative Inverse von q modul p berechnet.
	 * Danach werden m1 = y^dP (mod p) , m2 = y^dQ (mod q) berechnet.
	 * Danach h = (qInv *((m1-m2)+p))%p
	 * Der Klartext ergibt sich dann aus m2 + h * q
     *
	 * @param y Geheimtext der entschlüsselt werden soll.
	 * @param x Variable in die der Klartext nach der Entschlüsslung geschrieben wird.
	 * @return war die Entschlüsselung erfolgreich wird true zurückgegeben. War der Geheimtext größer n oder
	 * waren beiden Primzahlen gleich dann wird false zurückgegeben.
	 */
	bool crt(const Integer& y, Integer& x) const;
	/**
	 * Der Algorithmus von garner erlaubt eine effizientere Entschlüsselung eines mit RSA verschlüsselten Geheimtextes.
	 * Hierzu werden drei Hilfsvariablen benöigt:
	 * a = y^d mod p
	 * b = y^d mod q
	 * q_inverse = multiplikatives Inverses von q mod p
	 * mit der Formel x=(((a-b)*(q_Inv%p))%p)*q+b wird der Klartext x berechnet.
	 * @param y Geheimtext zum entschlüsseln
	 * @param x Hier wird der Entschlüsselte Klartext gespeichert.
	 * @return ist y nicht Element des Geheimtextraumes dann wird false zurückgegeben. Sonst wird der Geheimtext
	 * entschlüsselt und true wird zurückgegeben.
	 */
	bool garner(const Integer& y, Integer& x) const;
};

#endif /* RSADECRYPTOR_H_ */
