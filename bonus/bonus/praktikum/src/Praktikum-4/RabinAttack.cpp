/*
 * RabinAttack.cpp
 */

#include "PublicKeyAlgorithmBox.h"
#include "RabinDecryptor.h"
#include "RabinAttack.h"
#include "osrng.h"
#include <iostream>

using namespace std;

RabinAttack::RabinAttack() {
}

RabinAttack::~RabinAttack() {
}

int RabinAttack::factorize(const Integer& n, Integer& f, int max_tries,
		RabinDecryptor& rabin_dec) {
    PublicKeyAlgorithmBox pubk;
    NonblockingRng rng;
    Integer v; // Ergebnisse von Decrypt
    Integer x; // Ciphertext
    Integer r; // Plaintext Random
    Integer m1,m2; // Dummy zum aufrufen von EEA (Inverse)
    /**
     * Es wird ein zufälliger Klartext erzeugt und verschlüsselt. Danach wird dieser Entschlüsselt und das Ergebniss gespeichert.
     */
    for(int i=0;i<max_tries;i++){
        v=0;
        r.Randomize(rng, n.BitCount()-1); // Zufallsklartext
        x=(r*r)%n; // Verschlüsselt r
        rabin_dec.compute(x,v);
        /**
         * Ist das Ergebniss der Entschlüsselung kongruent zum Klartext oder -Klartext (mod n) dann wird das
         * Ergebniss verworfen und im nächsten Schleifendurchlauf ein neuer Versuch gestartet.
         * Ist dies nicht der Fall dann ist gcd(Ergebniss-Klartext,n) einer der Faktoren von n.
         * max_tries Versuche werden maximal durchgeführt.
         */
        if(v-r==0||v+r==n){
            continue;
        }
        else{
            pubk.EEA(v-r,n,f,m1,m2);
            return i+1;
        }
    }
  return -1;
}
