import sys
import math
n= int( sys.argv[1])
bound = int(math.sqrt(n))
q=3
while(n%q!=0 and q<=bound):
    q+=2
    if((q%1000001)==0):
        progress = q/bound
        print(str(round(progress*100,3))+"%")
p=int(n/q)
print("\nn factorized: \np:"+str(p)+"\nq:"+str(q))
