import sys
import math
import time
n= int( sys.argv[1])
#n= 1099532599387 #40 Bit Key
#n= 281476922870851 #48 Bit Key
#n= 18446744400127067027 # 64Bit Key

bound = int(math.sqrt(n))

q=3
timestamp= time.time()

while(n%q!=0 and q<=bound):
    q+=2
    progress = q/bound
    timestamp2 = time.time()
    if(timestamp2-timestamp>=1):
        print(str(round(progress*100,3))+"%")
        timestamp=timestamp2
p=int(n/q)
print("\nn factorized: \np:"+str(p)+"\nq:"+str(q))
